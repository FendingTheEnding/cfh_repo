Due Date:
	Aggro|Passive Speaks
	Object Scan Zoom
	Scan Sequence			03/28



CFH Action Plan:

Story:
	Outline flow given components
	Write next revision

Tools:
	Level Streaming
	Subtitle/Audio Menu
	

Interactive Dialogue System:
	Refactor Code
		Add
	HUD:
		If NPC notice object, track it
		Scan animation should be sequence var
		On screen location tracker
		Scan animation and object zoom
		
Level Creation:
	Arena collision juttering
	Finish hydro offices
		Ivy decoration? Texture properly, map lab and office
	
Substance:
	Tutorial to fully learn
	Properly texture opening scene
	
Blender:
	Second rev of opening assets
	
Animation:
	Creation of character arms and animate scenarios:
		Drill locks
		Connect wires
		Play audio clip?
	Create alliens and animations:
		Parent:
			Walk (Change rate for aggressive or not?)
			Attack sequence
		Child:
			Defensive stance
			Jump and face hug
			
			
To Ship ALPHA Test:
	Basic character model
		Arm Drill and suit look
	HUD object tracking and zoom (maybe zoom)
	Functioning doors on event
	Open panel animation
	Full test of communication system
	Light explore hydro story, unlock to vent to space
	Monitor dialogue selection for feedback
	Finish office layout and build