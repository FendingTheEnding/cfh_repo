// Kenneth Kratzer 2020

#pragma once

// Here since EActionType is used
#include "MyGameInstanceCpp.h"

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ActionSequenceSetter.generated.h"

// Forward Declarations

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CFH_CPP_API UActionSequenceSetter : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UActionSequenceSetter();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	int32 CurInc{ 0 };
	int32 NextInc{ 0 };
	int32 CurSeqCnt{ 0 };


	// CHECK AFTER
	// Set to game time action started
	float TimeOfAction{ 0.f };
	// Set to time it takes for selected action to perform
	float WaitDuration{ 0.f };

	bool RetargetAssignment{ false };


	// Current Sequence Values
	float SelectionInc;
	bool ActionOverride;
	float Delay;
	float ObjAnim;
	float BufferNum;
	float BufferDelay;
	float BufferRetarget;

	// Action Guide References
	int32 HexIdentifier;
	TArray<EActionType> ActionSequence;
	TArray<float> CurrentSequence;
	TArray<FString> NextSequence;
	TArray<FString> Dialogue;
	TArray<USoundWave*> AudioClips;
	TArray<UAnimSequence*> AnimationReferences;

	// Set to pointer of where owner hosts these values
	FString* ActiveDialogue{ nullptr };
	USoundWave** ActiveAudioClip{ nullptr };
	UAnimSequence** ActiveAnimation{ nullptr };

	UAnimationAsset* ObjectAnimation{ nullptr };

	// Set to pointer of where owner hosts these values
	int32* LocAggressiveAnim{ nullptr };
	int32* LocPassiveAnim{ nullptr };
	int32* LocPlayerSpeak{ nullptr };
	int32* LocPlayerAggroSpeak{ nullptr };
	int32* LocPlayerPassSpeak{ nullptr };
	int32* LocNPCSpeak{ nullptr };
	int32* LocNPCAggroSpeak{ nullptr };
	int32* LocNPCPassSpeak{ nullptr };

	// Pointer to ActionObjectModel ActiveSequenceNum value
	int32* ActiveSequenceNum{ nullptr };

	UPROPERTY()
	UMyGameInstanceCpp* GI = nullptr;


public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// Assign ActionGuide values and Reference Pointers to owner values that will be set throughout this class's functions
	void ActionSetterSetup(FActionGuide* ActionGuide, int32 *ActiveSequenceNumPtr,
		int32* LocAggressiveAnimPtr, int32* LocPassiveAnimPtr,
		int32* LocPlayerSpeakPtr, int32* LocPlayerAggroSpeakPtr, int32* LocPlayerPassSpeakPtr,
		int32* LocNPCSpeakPtr, int32* LocNPCAggroSpeakPtr, int32* LocNPCPassSpeakPtr,
		FString* ActiveDialoguePtr, USoundWave** ActiveAudioClipPtr, UAnimSequence** ActiveAnimationPtr);
		//TArray<float>& CurrentSequenceRef, TArray<EActionType>& ActionSequenceRef, TArray<FString>& DialogueRef, TArray<USoundWave*>& AudioClipsRef, TArray<UAnimSequence*>& AnimationReferencesRef);

	void SetSequenceIncs();
	void SetActiveSequenceValues(float ActionBufferTime);
		//, FString& ActiveDialogue, USoundWave*& ActiveAudioClip, UAnimSequence*& ActiveAnimation, 
		//			TArray<float> CurrentSequence, TArray<EActionType> ActionSequence, TArray<FString> Dialogue, TArray<USoundWave*> AudioClips, TArray<UAnimSequence*> AnimationReferences);
	void PrepSequenceActive(float ActionBufferTime);
	void SetSequenceActive();
	void SetNextSequenceOptions();// int32& LocAggressiveAnim, int32& LocPassiveAnim, int32& LocPlayerSpeak, int32& LocNPCSpeak, const int32& HexIdentifier,
					//const TArray<EActionType>& ActionSequence, const TArray<FString>& NextSequence);
	void RetargetCompAssignment(FString RetargetRef);// , int32& LocAggressiveAnim, int32& LocPassiveAnim, int32& LocPlayerSpeak, int32& LocNPCSpeak,
					//const int32& HexIdentifier, const TArray<EActionType>& ActionSequence, const TArray<FString>& NextSequence);

	UAnimationAsset* GetObjectAnimation();

	bool SequenceActive{ false };
	bool ScannableObject{ false };
	bool ObjectMarker{ false };
};
