// Kenneth Kratzer 2020

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "DynamicFirstPerson.generated.h"

// Forward Declarations
class UMyGameInstanceCpp;
class UCFH_AnimInstance;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CFH_CPP_API UDynamicFirstPerson : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UDynamicFirstPerson();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	void SetupInputComponent();
	TArray<FName> AxisToBind = { FName("Turn"), FName("LookUp"), FName("TurnAtRate"), FName("LookUpAtRate") };


	// Base turn rate, in deg/sec. Other scaling may affect final turn rate.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseTurnRate = 45.f;

	// Base look up/down rate, in deg/sec. Other scaling may affect final rate.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseLookUpRate = 45.f;

	void LookUpAtRate(float Rate);
	void TurnAtRate(float Rate);

	UPROPERTY()
	UMyGameInstanceCpp* GI = nullptr;
	/*
	UPROPERTY()
	APlayerController* MyController = nullptr;
	*/
	UPROPERTY()
	ACharacter* MyCharacter = nullptr;
	UPROPERTY()
	UCFH_AnimInstance* AnimInstance = nullptr;

	// Used to hold value of how far the camera has moved compared to the body mesh
	float DegMoved{ 0.f };

	float AnimMaxFlipped{ false };
	float AnimMinFlipped{ false };
	float AnimYawMax{ 0.f };
	float AnimYawMin{ 0.f };
	float AnimPitchMax{ 0.f };
	float AnimPitchMin{ 0.f };

	float InitialRotationDifference{ 0 };

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void SetAnimationLookValues(FVector AnimationCharacterLocation, FRotator AnimationCharacterRotation, float IntendedCameraYaw, float IntendedCameraPitch, float YawOffset, float PitchOffset);
	FVector AnimLockedCharacterLocation;
	FRotator AnimLockedCharacterRotation;
};
