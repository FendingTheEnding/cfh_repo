// Kenneth Kratzer 2020

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "CFH_AnimInstance.generated.h"

/**
 * 
 */

// Enum for Animation Stance
UENUM()
enum class EAnimStance : uint8
{
	Passive,
	Aggressive
};

// Enum for Action Animation
UENUM()
enum class EActionAnim : uint8
{
	Active,
	Inactive
};

UENUM()
enum class ETurnAnim : uint8
{
	Left,
	Right,
	Inactive
};

// Forward Declarations
class ACFH_PlayerCharacter;

UCLASS()
class CFH_CPP_API UCFH_AnimInstance : public UAnimInstance
{
	GENERATED_BODY()
	
protected:
	// Called at start of play
	virtual void NativeBeginPlay();

	// Called every frame
	virtual void NativeUpdateAnimation(float DeltaTimeX) override;

	// Used to signal when character stance should change
	UPROPERTY(BlueprintReadOnly, Category = "State")
	EAnimStance AnimStance = EAnimStance::Passive;
	// Used to signal when object assigned animation should start
	UPROPERTY(BlueprintReadOnly, Category = "State")
	EActionAnim ActionAnim = EActionAnim::Inactive;
	// Used to single turn animation
	UPROPERTY(BlueprintReadOnly, Category = "State")
	ETurnAnim TurnAnim = ETurnAnim::Inactive;

	// Used for Animation Blend Space
	UPROPERTY(BlueprintReadOnly)
	FVector CharVel;
	// Used for Animation Blend Space
	UPROPERTY(BlueprintReadOnly)
	FRotator CharRot;

	// Sets input for stance changes
	void SetupInputComponent();
	// Called to stop one off animation and return to normal blendspace animations
	void EndActionAnim();

	// Called when stance change button is pressed to rotate between passive and aggressive stances
	void SetAnimStance();

public:
	UCFH_AnimInstance();

	EAnimStance GetAnimStance() const;

	EActionAnim GetActionAnim() const;
	// Will change EActionAnim to perform one off animations and set timer to call EndActionAnim based on SequenceDuration
	void StartActionAnim(float SequenceDuration);

	ETurnAnim GetTurnAnim() const;
	void SetTurnAnim(int32 State = -1);

};
