// Kenneth Kratzer 2020

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Runtime/Engine/Classes/Engine/DataTable.h"
#include "CFH_PlayerCharacter.generated.h"

/**
 * Character built on initial UE First Person Character
 */

// Forward Declarations
class AActionObjectModel;
class UCFH_CharacterActionHandler;
class UDynamicFirstPerson;
class UInViewportHandler;
class UInteractiveObjectDialogueSpeaker;
class UMyGameInstanceCpp;

UCLASS()
class CFH_CPP_API ACFH_PlayerCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	//UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	//class USkeletalMeshComponent* Mesh1P;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FirstPersonCameraComponent;
	/** In Viewport Handler for objects to see if they are in view **/
	UPROPERTY(VisibleAnywhere, Category = "Components")
	UInViewportHandler* InViewportHandler = nullptr;
	/** Dynamic First Person rewrites view axis to limit character movement vs camera **/
	UPROPERTY(VisibleAnywhere, Category = "Components")
	UDynamicFirstPerson* DynamicFirstPerson = nullptr;
	/** Speaker for character audio*/
	UPROPERTY(VisibleAnywhere, Category = "Components")
	UInteractiveObjectDialogueSpeaker* InteractiveObjectDialogueSpeaker = nullptr;
	/** Dynamic First Person rewrites view axis to limit character movement vs camera **/
	UPROPERTY(VisibleAnywhere, Category = "Components")
	UCFH_CharacterActionHandler* CharacterActionHandler = nullptr;
	/** Animation to be used for object to call new animation*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animation, meta = (AllowPrivateAccess = "true"))
	class UAnimSequence* ActionAnim;

private:
	FString ContextString;

public:
	// Sets default values for this actor's properties
	ACFH_PlayerCharacter();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Locks in object for interaction
	void AssignCurrentObject(AActionObjectModel* ObjectToBeAssigned);

	// Calls InteractiveObjectDialogueSpeaker play audio from player or object if requested
	void CallAudioInput(bool ObjectRequest = false);

	// Called by object after character has been moved to proper position
	void CallAnimAction();

	// Set properties for starting object scan
	void SetScanMPC(FVector ObjectLocation);

protected:
	virtual void BeginPlay() override;
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	// When player calls audio this passes to CallAudioInput; done since it has bool var for NPCSpeak call
	void PassToAudioInput();
	
	// Handles moving forward/backward
	void MoveForward(float Val);

	// Handles stafing movement, left and right
	void MoveRight(float Val);

	// Called via input to turn at a given rate.
	// @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	void TurnAtRate(float Rate);

	// Called via input to turn look up/down at a given rate.
	// @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	void LookUpAtRate(float Rate);

	// Base turn rate, in deg/sec. Other scaling may affect final turn rate.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseTurnRate;
	
	// Base look up/down rate, in deg/sec. Other scaling may affect final rate.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseLookUpRate;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animation")
	class UCFH_AnimInstance* AnimInst;

	// Scanning Material Paramater
	UPROPERTY()
	UMaterialParameterCollection* ScanMPC{ nullptr };

	// Holds Animation Instance
	UPROPERTY()
	UAnimInstance* AnimIn = nullptr;

	// Holds Game Instance for global var
	UPROPERTY()
	UMyGameInstanceCpp* GI = nullptr;
	
	// Holds VPRA of object player is currently interacting with
	UPROPERTY()
	AActionObjectModel* CurrentlyActiveObject = nullptr;

};
