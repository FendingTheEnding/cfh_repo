// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Runtime/Engine/Classes/Engine/DataTable.h"
#include "MyGameInstanceCpp.generated.h"

/**
 * 
 */

UENUM()
enum class EActionType : uint8
{
	StartStage,
	AggressiveAnim,
	PassiveAnim,
	PlayerSpeak,
	PlayerAggroSpeak,
	PlayerPassSpeak,
	NPCSpeak,
	NPCAggroSpeak,
	NPCPassSpeak,
	LevelWait,
	EndStage
};

// Forward Declaration
class AActionObjectModel;
class UActionSequenceSetter;

USTRUCT(BlueprintType)
struct FObjectInnerArr
{
	GENERATED_BODY()

public:
	UPROPERTY()
	TArray<float> InnerArray;
};

USTRUCT(BlueprintType)
struct FActionGuide : public FTableRowBase
{
	GENERATED_BODY()
public:
	// Reference Number of Component
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString ReferenceNumber;
	// Type of Action being performed
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<EActionType> ActionSequence;
	// [Alter delay to next action, Buffer location, Delay to Buffer launch, Skip X sequences on Buffer]
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<float> CurSequence;
	// [Alter delay to next action, Buffer location, Delay to Buffer launch, Skip X sequences on Buffer]
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FString> NextSequence;
	// Anim Sequence References
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<UAnimSequence*> AnimRefs;
	// Dialogue
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FString> Dialogue;
	// Audio Clips 
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<USoundWave*> AudioClips;
	// Buffer Dialogue
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FString> BufferDialogue;
	// Buffer Clips
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<USoundWave*> BufferClips;
};

UCLASS()
class CFH_CPP_API UMyGameInstanceCpp : public UGameInstance
{
	GENERATED_BODY()

private:
	FString ContextString;
	
public:
	UMyGameInstanceCpp(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditAnywhere)
	float IsSpeakActive = 0.f;

	void SetSpeak(float val);

	// Returns action guide to each VPRA component
	FActionGuide* GetRequestedReference(FName StaticMeshName, UActionSequenceSetter* RequestingActor);// , FVector MeshLocation, FRotator MeshRotation, float SpeakDistance);

	// To store number for lookup of ViewPosRelActArr
	UPROPERTY()
	TArray<int32> ReferenceNumArr;
	/*
	// To allow communication between VPRA components through character
	UPROPERTY()
	TArray<UViewPositionRelativeAction*> ViewPosRelActArr;
	*/
	// To allow communication between VPRA components through character
	UPROPERTY()
	TArray<UActionSequenceSetter*> ActionObjectArray;
	// To store location and rotation of each VPRA object
	UPROPERTY()
	TArray<FObjectInnerArr> ObjectLocationRotationArr;

	// Table of objects and corresponding action sequences
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Custom")
	class UDataTable* ActionGuideDT;

	void AssignCurrentObject(AActionObjectModel* ObjectToBeAssigned);
	// Holds VPRA of object player is currently interacting with
	UPROPERTY()
	AActionObjectModel* CurrentlyActiveObject = nullptr;

};
