// Kenneth Kratzer 2020

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ActionObjectVisualEffectsHandler.generated.h"

// Enum for Widget Visibility
UENUM()
enum class EWidgetVisibility : uint8
{
	Unseen,
	Seen
};

// Forward Declarations
class AActionObjectModel;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CFH_CPP_API UActionObjectVisualEffectsHandler : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UActionObjectVisualEffectsHandler();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	// Owning parent object
	UPROPERTY(VisibleAnywhere, Category = "Components")
	AActionObjectModel* ActionObjectModel { nullptr };

	// Scanning Material Paramater
	UPROPERTY()
	UMaterialParameterCollection* ScanMPC {	nullptr	};

	// Used to say if interpolation of scan effect should run in tick
	bool ScanningObject{ false };
	// Location to set start of scan effect to
	void SetScanMPC(FVector ObjectLocation);
	// Function to stop scanning effect
	void FinishScan();

	// Time used to set when scan should stop
	UPROPERTY(EditAnywhere, Category = "Setup")
	float ScanTime{ 8.f };

	// Variable checked to see if object is in view and active or not
	UPROPERTY(BlueprintReadOnly, Category = "State")
	EWidgetVisibility VisibilityState = EWidgetVisibility::Unseen;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// Called to set variables and start scan effect
	void ScanObject();

	// To retrieve State for widget BP
	EWidgetVisibility GetVisibilityState() const;

	// Called to set state of widget visibility
	void SetWidgetVisibility(bool bValue);
};
