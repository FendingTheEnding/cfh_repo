// Kenneth Kratzer 2020

#pragma once

// Included to allow reference of FActionGuide
#include "MyGameInstanceCpp.h"

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ActionGuideRetriever.generated.h"

// Forward Declarations
class UMyGameInstanceCpp;
// TODO: Will be replaced
class UActionObjectModel;
class UActionSequenceSetter;
//class UViewPositionRelativeAction;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CFH_CPP_API UActionGuideRetriever : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UActionGuideRetriever();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// Is used to make sure action guide values are properly filled and won't result in a crash of the game
	bool ActionGuideValidation(FName ObjectName, TArray<float> CurrentSequence, TArray<FString> NextSequence, 
		int32 ActionSequenceNum, int32 DialogueNum, int32 AudioClipsNum, int32 BufferDialogueNum, int32 BufferClipsNum);
	// Grabs action guide according to name of object and validates it before returning the guide to the action object
	FActionGuide* GetActionGuide(FName ObjectName, UActionSequenceSetter* ObjectSequenceSetter, UMyGameInstanceCpp* GI);
};
