// Kenneth Kratzer 2020

#pragma once

#include "CoreMinimal.h"
#include "Components/AudioComponent.h"
#include "Runtime/Engine/Classes/Engine/DataTable.h"
#include "InteractiveObjectDialogueSpeaker.generated.h"

/**
 * Handles dialogue audio directly related to interactive objects in the environment
 */

// Forward Declarations

// DataTable Struct
USTRUCT(BlueprintType)
struct FDialogueTable : public FTableRowBase
{
	GENERATED_BODY()
public:
	
	// Speaker 
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString SpeakerType;
	// [Num corresponding to which audio clip action should be played with, 0 - at audio start| 1 - at audio end]
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<int32> AnimationPlacement;
	// Dialogue 
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FString> Dialogue;
	// Audio Clips 
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<USoundWave*> AudioClips;
};

UCLASS( ClassGroup = (Custom), meta = (BlueprintSpawnableComponent) )
class CFH_CPP_API UInteractiveObjectDialogueSpeaker : public UAudioComponent
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	UInteractiveObjectDialogueSpeaker();

	void PlayObjectAudio(FString DialogueText, USoundWave* AudioClip, bool Override);

	
	float LastSpeakTime = 0.f;
	float WaitToSpeakTime = 0.f;
// private:

// protected:
 
};
