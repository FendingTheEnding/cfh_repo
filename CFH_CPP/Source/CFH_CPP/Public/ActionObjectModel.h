// Kenneth Kratzer 2020

#pragma once

// Included to allow reference of EActionType
#include "MyGameInstanceCpp.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ActionObjectModel.generated.h"

// Forward Declaration
class UActionGuideRetriever;
class UActionSequenceSetter;
class UActionSequenceMonitor;
class ACharacter;
class ACFH_PlayerCharacter;
class UCFH_CharacterActionHandler;
class UInViewportHandler;
class USoundWave;

UCLASS()
class CFH_CPP_API AActionObjectModel : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AActionObjectModel();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Base component so positioning of other components can change
	UPROPERTY(VisibleAnywhere, Category = "Components")
	USceneComponent* SceneRoot = nullptr;
	// Used to get Action Guide for object
	UPROPERTY(VisibleAnywhere, Category = "Components")
	UActionGuideRetriever* ActionGuideRetriever = nullptr;
	// Looks for player or object actions to execute according to set actions
	UPROPERTY(VisibleAnywhere, Category = "Components")
	UActionSequenceMonitor* ActionSequenceMonitor = nullptr;
	// Controls sequencing of actions; Sets active and what to look for next
	UPROPERTY(VisibleAnywhere, Category = "Components")
	UActionSequenceSetter* ActionSequenceSetter = nullptr;

	// Components referenced from this class
	UPROPERTY()
	UMyGameInstanceCpp* GI = nullptr;
	UPROPERTY()
	ACharacter* MyCharacter = nullptr;
	UPROPERTY()
	UInViewportHandler* InViewportHandler = nullptr;
	UPROPERTY()
	UCFH_CharacterActionHandler* CharacterActionHandler = nullptr;

	// Used by ActionGuide components to reference location of Active Sequence
	int32 ActiveSequenceNum{ -1 };
	// Assigned by subclass and used to select row of action guide needed
	UPROPERTY(EditAnywhere, Category = "Setup")
	FName ActionGuideName{ "" };


	// Used to set which actions are options in a given sequence; -1 is not an option otherwise set to sequences location in action guide
	int32 LocAggressiveAnim{ -1 };
	int32 LocPassiveAnim{ -1 };
	int32 LocPlayerSpeak{ -1 };
	int32 LocPlayerAggroSpeak{ -1 };
	int32 LocPlayerPassSpeak{ -1 };
	int32 LocNPCSpeak{ -1 };
	int32 LocNPCAggroSpeak{ -1 };
	int32 LocNPCPassSpeak{ -1 };

	// Used as base check for if object can operate
	bool EndAction{ true };
	// Standard time to wait inbetween each action
	UPROPERTY(EditAnyWhere, Category = "Setup")
	float ActionBufferTime{ 0.2f };

	// Time SequenceActive was last true
	UPROPERTY(EditAnyWhere, Category = "Setup")
	float TimeOfAction{ 0.f };

	// Called when ActionRequest is returned true
	void PreActionPrep();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	// Used to set Character position during animation; Set 1 for axis in line with object, actual position for other axis, and 0 for Z.
	UPROPERTY(EditAnywhere, Category = "Setup")
	FVector CharacterPositioning{ 0, 0, 0 };
	// Only uses Z value to adjust rotation character faces
	UPROPERTY(EditAnywhere, Category = "Setup")
	FRotator CharacterRotation{ 0, 0, 0 };
	// Set Y and Z to lock max/min values for look pitch and yaw during animation
	UPROPERTY(EditAnywhere, Category = "Setup")
	FRotator CharacterRotationOffset{ 0, 0, 0 };
	
	// To say if action can cut off previous action
	bool ActionOverride{ false };
	// Contains current dialogue string
	FString ActiveDialogue;
	// Contains current audio clip to be played
	USoundWave* ActiveAudioClip{ nullptr };
	// Contains current animaiton to be played
	UAnimSequence* ActiveAnimation{ nullptr };


	// Called from CharacterActionHandler; Checks ActionSequenceMonitor for validity of requested action and plays action prep if true
	bool PlayerActionRequest(EActionType RequestedAction);
	// Called from CharacterActionHandler; Checks ActionSequenceMonitor for validity of requested action and plays action prep if true
	bool ObjectActionRequest(EActionType RequestedAction);
	// Finds location animating character should have prior to animation being called
	FVector GetAnimationPositioning();
	// Hosts code to perform on animation; Left blank and to be used by sub-classes
	virtual void PerformOnAnim();
	// For use by subclasses; Will set render depth depending on main mesh type used by subclass
	virtual void SetRenderDepth(bool bValue) const;

};
