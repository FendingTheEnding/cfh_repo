// Kenneth Kratzer 2020

#pragma once

// Included to allow reference of EActionType
#include "MyGameInstanceCpp.h"

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ActionSequenceMonitor.generated.h"

// Forward Declaration
class UActionSequenceSetter;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CFH_CPP_API UActionSequenceMonitor : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UActionSequenceMonitor();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	// Set pointer to owner's memory location of these values
	int32* LocAggressiveAnim{ nullptr };
	int32* LocPassiveAnim{ nullptr };
	int32* LocPlayerSpeak{ nullptr };
	int32* LocPlayerAggroSpeak{ nullptr };
	int32* LocPlayerPassSpeak{ nullptr };
	int32* LocNPCSpeak{ nullptr };
	int32* LocNPCAggroSpeak{ nullptr };
	int32* LocNPCPassSpeak{ nullptr };

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// Called by owner to set pointers
	void ActionMonitorSetup(int32* LocAggressiveAnimPtr, int32* LocPassiveAnimPtr,
		int32* LocPlayerSpeakPtr, int32* LocPlayerAggroSpeakPtr, int32* LocPlayerPassSpeakPtr,
		int32* LocNPCSpeakPtr, int32* LocNPCAggroSpeakPtr, int32* LocNPCPassSpeakPtr);

	// Returns if requested action is a valid option and sets ActiveSequenceNum to correct value if true
	bool PlayerActionRequest(EActionType RequestedAction, int32& ActiveSequenceNum, bool ObjectInViewport);
	// Returns if requested action is a valid option and sets ActiveSequenceNum to correct value if true
	bool ObjectActionRequest(EActionType RequestedAction, int32& ActiveSequenceNum);

};
