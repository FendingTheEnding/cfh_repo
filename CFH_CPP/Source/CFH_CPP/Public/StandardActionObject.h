// Kenneth Kratzer 2020

#pragma once

#include "CoreMinimal.h"
#include "ActionObjectModel.h"
#include "StandardActionObject.generated.h"

// Forward Declaration
class UActionObjectVisualEffectsHandler;

/**
 * 
 */
UCLASS()
class CFH_CPP_API AStandardActionObject : public AActionObjectModel
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	AStandardActionObject();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	bool ThisInAction{ false };
	UPROPERTY(EditAnywhere, Category = "Setup")
	int32 SpeakDistance{ 275 };


	/** Handles all visual effects on object **/
	UPROPERTY(VisibleAnywhere, Category = "Components")
	UActionObjectVisualEffectsHandler* ActionObjectVisualEffectsHandler { nullptr };

	// Required for widget to work
	UFUNCTION(BlueprintImplementableEvent, Category = "Setup")
	void GetVisualEffectsHandler(UActionObjectVisualEffectsHandler* VisualEffectsHandlerCompRef);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
