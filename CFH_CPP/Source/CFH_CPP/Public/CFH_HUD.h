// Kenneth Kratzer 2020

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "CFH_HUD.generated.h"

/**
 * 
 */
UCLASS()
class CFH_CPP_API ACFH_HUD : public AHUD
{
	GENERATED_BODY()

public:
	ACFH_HUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;
	
};
