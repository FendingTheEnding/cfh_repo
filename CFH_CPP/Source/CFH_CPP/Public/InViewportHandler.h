// Kenneth Kratzer 2020

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "InViewportHandler.generated.h"

// Forward Declaration
class ACFH_PlayerController;
class UMyGameInstanceCpp;


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CFH_CPP_API UInViewportHandler : public UActorComponent
{
	GENERATED_BODY()

private:	
	// Holds Game Instance for global var
	UPROPERTY()
	UMyGameInstanceCpp* GI = nullptr;
	UPROPERTY()
	ACFH_PlayerController* MyController = nullptr;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	// Sets default values for this component's properties
	UInViewportHandler();

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	//void FindInViewport(FVector ViewpointLocation, FRotator ViewpointRotation);
	// Find distance between player and object
	bool ViewDistance(FVector ViewpointLocation, TArray<float> ObjectLocationRotation);
	// Find Yaw of camera relative to object
	float CameraTraceYawAngle(FVector CameraTraceLocation, FRotator CameraTraceRotation, FVector ObjectLocation);
	// Find Pitch of camera relative to object
	float CameraTracePitchAngle(FVector CameraTraceLocation, FRotator CameraTraceRotation, FVector ObjectLocation);
	// Finds if object is in view port based on angle of camera to object
	bool ObjectInViewport(FVector ObjectLocation);
		
	bool ViewWithinDistance(FVector ObjectLocation, float InteractDistance);
};
