// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "CFH_CPPGameMode.h"
#include "CFH_CPPHUD.h"
#include "CFH_CPPCharacter.h"
#include "UObject/ConstructorHelpers.h"

ACFH_CPPGameMode::ACFH_CPPGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ACFH_CPPHUD::StaticClass();
}
