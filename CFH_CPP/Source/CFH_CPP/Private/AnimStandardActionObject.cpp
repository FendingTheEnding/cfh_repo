// Kenneth Kratzer 2020


#include "AnimStandardActionObject.h"
#include "ActionSequenceSetter.h"

AAnimStandardActionObject::AAnimStandardActionObject()
{
	SkeletalMeshObject = CreateDefaultSubobject<USkeletalMeshComponent>(FName("Skeletal Mesh Object"));
	SkeletalMeshObject->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

void AAnimStandardActionObject::BeginPlay()
{
	//SkeletalMeshObject->Get;
	//ActionGuideName = FName(*StaticMeshObject->GetStaticMesh()->GetName());
	if (ActionGuideName == FName(""))
	{
		UE_LOG(LogTemp, Error, TEXT("AnimStandardActionObject has blank ActionGuideName Value"));
	}
	else
	{
		Super::BeginPlay();
	}
}

void AAnimStandardActionObject::PerformOnAnim()
{
	if (ActionSequenceSetter->GetObjectAnimation())
	{
		SkeletalMeshObject->PlayAnimation(ActionSequenceSetter->GetObjectAnimation(), false);
	}
	Super::PerformOnAnim();
}

void AAnimStandardActionObject::SetRenderDepth(bool bValue) const
{
	SkeletalMeshObject->SetRenderCustomDepth(bValue);
}