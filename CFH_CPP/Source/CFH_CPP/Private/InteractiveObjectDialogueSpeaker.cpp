// Kenneth Kratzer 2020


#include "InteractiveObjectDialogueSpeaker.h"
#include "UObject/ConstructorHelpers.h"
#include "Engine/DataTable.h"


UInteractiveObjectDialogueSpeaker::UInteractiveObjectDialogueSpeaker()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	
	// ...

}

void UInteractiveObjectDialogueSpeaker::PlayObjectAudio(FString DialogueText, USoundWave* AudioClip, bool Override = false)
{
	if ((GetWorld()->GetTimeSeconds() - LastSpeakTime) > WaitToSpeakTime || Override)
	{
		if (AudioClip)
		{
			UE_LOG(LogTemp, Warning, TEXT("Audio is being played"));
			// 0.2f was googled average time between moments in conversation so left as hard code
			WaitToSpeakTime = AudioClip->GetDuration() + 0.2f;
			UE_LOG(LogTemp, Error, TEXT("WaitToSpeakTime: %f"), AudioClip->GetDuration());
			FSubtitleCue ToCue;
			ToCue.Text = FText::FromString(DialogueText);
			UE_LOG(LogTemp, Warning, TEXT("Dialogue: %s"), *DialogueText)
			ToCue.Time = 0.0f;
			AudioClip->Subtitles.Add(ToCue);
			SetSound(AudioClip);
			Play();
			LastSpeakTime = GetWorld()->GetTimeSeconds();
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("AudioClip Invalid"))
		}
	}
}
