// Kenneth Kratzer 2020


#include "StandardActionObject.h"
#include "ActionObjectVisualEffectsHandler.h"
#include "ActionSequenceMonitor.h"
#include "ActionSequenceSetter.h"
#include "CFH_CharacterActionHandler.h"
#include "InViewportHandler.h"

AStandardActionObject::AStandardActionObject()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ActionObjectVisualEffectsHandler = CreateDefaultSubobject<UActionObjectVisualEffectsHandler>(FName("Action Object Visual Effects Handler"));
}

// Called when the game starts or when spawned
void AStandardActionObject::BeginPlay()
{
	Super::BeginPlay();

	GetVisualEffectsHandler(ActionObjectVisualEffectsHandler);

}

// Called every frame
void AStandardActionObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!EndAction)
	{
		if (!ThisInAction)
		{
			if (InViewportHandler->ViewWithinDistance(GetActorLocation(), SpeakDistance))
			{
				if (InViewportHandler->ObjectInViewport(GetActorLocation()))
				{
					// Set all actives for regular functions
					if (GI->IsSpeakActive == 0.f)
					{
						GI->SetSpeak(1.f);
						ThisInAction = true;
						GI->AssignCurrentObject(this);
						ActionObjectVisualEffectsHandler->SetWidgetVisibility(true);
						UE_LOG(LogTemp, Warning, TEXT("Object Set Active"))
					}
				}
			}
		}
		else
		{
			if (!ActionSequenceSetter->SequenceActive)
			{
				if (LocNPCSpeak != -1 || LocNPCAggroSpeak != -1 || LocNPCPassSpeak != -1)
				{
					CharacterActionHandler->CallAudioInput(true);
				}
				else if (ActiveSequenceNum == -8888)
				{
					EndAction = true;
					ThisInAction = false;
					GI->SetSpeak(0.f);
					ActionObjectVisualEffectsHandler->SetWidgetVisibility(false);
					UE_LOG(LogTemp, Error, TEXT("%s: EndStage"), *ActionGuideName.ToString());
				}
				else if (!InViewportHandler->ObjectInViewport(GetActorLocation()) && GetWorld()->TimeSeconds > TimeOfAction)
				{
					ThisInAction = false;
					GI->SetSpeak(0.f);
					ActionObjectVisualEffectsHandler->SetWidgetVisibility(false);
					UE_LOG(LogTemp, Error, TEXT("%s: Inactive"), *ActionGuideName.ToString());
				}
			}
			else
			{
				// Used to set scan if needed when Sequence is active
				if (ActionSequenceSetter->ScannableObject)
				{
					UE_LOG(LogTemp, Error, TEXT("Scanning"))
					ActionObjectVisualEffectsHandler->ScanObject();
					ActionSequenceSetter->ScannableObject = false;
				}
				// FIX: Replace with float InactiveBufferTime
				TimeOfAction = GetWorld()->TimeSeconds + 1.5f;
			}
		}
	}
}