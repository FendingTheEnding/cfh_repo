// Kenneth Kratzer 2020


#include "ActionGuideRetriever.h"
#include "ActionObjectModel.h"
#include "MyGameInstanceCpp.h"

// Sets default values for this component's properties
UActionGuideRetriever::UActionGuideRetriever()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UActionGuideRetriever::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UActionGuideRetriever::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

FActionGuide* UActionGuideRetriever::GetActionGuide(FName ObjectName, UActionSequenceSetter* ObjectSequenceSetter, UMyGameInstanceCpp* GI)
{
	FActionGuide* ActionGuide = GI->GetRequestedReference(ObjectName, ObjectSequenceSetter);
	if (!ActionGuide)
	{
		UE_LOG(LogTemp, Error, TEXT("%s: Action Guide not loaded!!"), *ObjectName.ToString());
		return nullptr;
	}
	else
	{
		// FIX: For dev purposes
		UE_LOG(LogTemp, Warning, TEXT("%i, %s"), ActionGuide->AudioClips.Num(), *ObjectName.ToString());
		// Checks that guide values are set such that it won't crash game, i.e. there weren't errors in guide
		if (!ActionGuideValidation(ObjectName, ActionGuide->CurSequence, ActionGuide->NextSequence, ActionGuide->ActionSequence.Num(), ActionGuide->Dialogue.Num(), ActionGuide->AudioClips.Num(), ActionGuide->BufferDialogue.Num(), ActionGuide->BufferClips.Num()))
		{
			UE_LOG(LogTemp, Error, TEXT("%s: Failed Action Guide Validation"), *ObjectName.ToString());
			return nullptr;
		}
		else
		{
			return ActionGuide;
		}
	}
}

bool UActionGuideRetriever::ActionGuideValidation(FName ObjectName, TArray<float> CurrentSequence, TArray<FString> NextSequence, 
	int32 ActionSequenceNum, int32 DialogueNum, int32 AudioClipsNum, int32 BufferDialogueNum, int32 BufferClipsNum)
{
	// Set incrementors
	int32 CurCheck = 0;
	int32 NextCheck = 0;

	// Set inc to determine how many variables there are before '-8888.f', which is used to signal start of next sequence
	while (CurrentSequence[CurCheck] != -8888.f)
	{
		CurCheck += 1;
	}
	// Account for sentinel value of -8888.f
	CurCheck += 1;

	// Find number of sequences by incrementing for every 'End' in array; 'End' is used to signal start of next sequence
	for (int32 i = 0; i < NextSequence.Num(); i++)
	{
		if (NextSequence[i] == FString("End"))
		{
			NextCheck += 1;
		}
	}

	// Checks that number of values match what is expected based on number of Action Sequences
	if (ActionSequenceNum == (CurrentSequence.Num() / CurCheck) && ActionSequenceNum == NextCheck)
	{
		// Make sure the number of subtitles equals the number of audio clips
		if (DialogueNum == AudioClipsNum)
		{
			// Make sure the number of subtitles equals the number of buffer audio clips
			if (BufferDialogueNum == BufferClipsNum)
			{
				return true;
			}
			else
			{
				UE_LOG(LogTemp, Error, TEXT("%s: Failed BufferClips/BufferDialogue Check"), *ObjectName.ToString());
			}
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("%s: Failed AudioClips/Dialogue Check"), *ObjectName.ToString());
		}
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("%s: Failed ActionSequence.Num() Checks"), *ObjectName.ToString());
	}

	return false;
}