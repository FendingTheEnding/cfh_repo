// Kenneth Kratzer 2020


#include "CFH_PlayerCharacter.h"
#include "ActionObjectModel.h"
#include "ActionSequenceSetter.h"
#include "Animation/AnimInstance.h"
#include "Animation/AnimBlueprint.h"
#include "CFH_AnimInstance.h"
#include "CFH_CharacterActionHandler.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "DynamicFirstPerson.h"
#include "Engine/DataTable.h"
#include "InViewportHandler.h"
#include "InteractiveObjectDialogueSpeaker.h"
#include "Materials/MaterialParameterCollection.h"
#include "Materials/MaterialParameterCollectionInstance.h"
#include "MyGameInstanceCpp.h"
#include "Runtime/Engine/Classes/Kismet/KismetMaterialLibrary.h"


// Sets default values for this actor's properties
ACFH_PlayerCharacter::ACFH_PlayerCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// Set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	/*
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->SetRelativeRotation(FRotator(0.0f, 5.53f, -89.3f));
	Mesh1P->SetRelativeLocation(FVector(-0.4f, -11.4f, -115.0f));
	*/

	// Create speaker component used for all audio
	InteractiveObjectDialogueSpeaker = CreateDefaultSubobject<UInteractiveObjectDialogueSpeaker>(FName("Interactive Object Dialogue Speaker"));
	InViewportHandler = CreateDefaultSubobject<UInViewportHandler>(FName("In Viewport Handler"));
	DynamicFirstPerson = CreateDefaultSubobject<UDynamicFirstPerson>(FName("Dynamic First Person"));
	CharacterActionHandler = CreateDefaultSubobject<UCFH_CharacterActionHandler>(FName("Character Action Handler"));
	// MOVE
	// Assigns Animation Blueprint that gives all base motion animations
	static ConstructorHelpers::FObjectFinder<UAnimBlueprint> AnimBPObj(TEXT("AnimBlueprint'/Game/MyContent/Imports/Glycon/Animations/CFH_AnimationBP.CFH_AnimationBP'")); 
	if (AnimBPObj.Succeeded())
	{
		GetMesh()->SetAnimInstanceClass(AnimBPObj.Object->GeneratedClass);
	}
	// MOVE
	// Grab stand in animation that is used to be overriden with one time actions to blend into standard motion
	static ConstructorHelpers::FObjectFinder<UAnimSequence> AnimSeqObj(TEXT("AnimSequence'/Game/MyContent/Imports/Glycon/Animations/Idle.Idle'"));
	if (AnimSeqObj.Succeeded())
	{
		ActionAnim = AnimSeqObj.Object;
	}
	// CHANGE: Move to ObjectScannerComponent
	// Gets DataTable containting reference information for all interactable objects
	static ConstructorHelpers::FObjectFinder<UMaterialParameterCollection> ScanMPCObject(TEXT("MaterialParameterCollection'/Game/MyContent/Materials/PC_ScanRadius.PC_ScanRadius'"));
	if (ScanMPCObject.Succeeded())
	{
		ScanMPC = ScanMPCObject.Object;
		//ScanMPCI = GetWorld()->GetParameterCollectionInstance(ScanMPC);
	}
}

void ACFH_PlayerCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	// Assign game instance and animation instance
	GI = Cast<UMyGameInstanceCpp>(GetWorld()->GetGameInstance());
	AnimInst = Cast<UCFH_AnimInstance>(GetMesh()->GetAnimInstance());
	if (!GI || !AnimInst)
	{
		UE_LOG(LogTemp, Error, TEXT("Check GameInstance and AnimInstance gets loaded in CFH_PlayerCharacter"));
	}
	// CHANGE: Adjust appropriately for reworking
	// Assign initial object for variable lookups throughout functions
	//AssignCurrentObject(GI->ViewPosRelActArr[0]);
	AssignCurrentObject(Cast<AActionObjectModel>(GI->ActionObjectArray[0]->GetOwner()));
	// Assign initial object for variable lookups throughout functions
	//GI->AssignCurrentObject(GI->ViewPosRelActArr[0]);
	GI->AssignCurrentObject(Cast<AActionObjectModel>(GI->ActionObjectArray[0]->GetOwner()));
}

// Called every frame
void ACFH_PlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	

	// CHANGE: Move to ObjectScannerComponent
	/*
	if (CurrentlyActiveObject->ScannableObject)
	{
		float CurrentRadius = UKismetMaterialLibrary::GetScalarParameterValue(GetWorld(), ScanMPC, FName("Radius"));
		UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), ScanMPC, FName("Radius"), FMath::FInterpTo(CurrentRadius, 1, GetWorld()->GetDeltaSeconds(), 0.05));
	}
	*/
}
// MOVE: Set in GameInstance
void ACFH_PlayerCharacter::AssignCurrentObject(AActionObjectModel* ObjectToBeAssigned)
{
	CurrentlyActiveObject = ObjectToBeAssigned;
	UE_LOG(LogTemp, Warning, TEXT("CurrentlyActiveObject succeeded!!"));
}

// CHANGE: Move to ObjectScannerComponent
void ACFH_PlayerCharacter::SetScanMPC(FVector ObjectLocation)
{
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), ScanMPC, FName("Radius"), 0);
	UKismetMaterialLibrary::SetVectorParameterValue(GetWorld(), ScanMPC, FName("ObjectLocation"), ObjectLocation);
}

void ACFH_PlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &ACFH_PlayerCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ACFH_PlayerCharacter::MoveRight);

	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	// FIX: this has been changed and should be simplified
	PlayerInputComponent->BindAxis("Turn", this, &ACFH_PlayerCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("TurnRate", this, &ACFH_PlayerCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &ACFH_PlayerCharacter::LookUpAtRate);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ACFH_PlayerCharacter::LookUpAtRate);

	UE_LOG(LogTemp, Warning, TEXT("Axis Bound"))
}
// CHANGE: Move to AnimationHandler Compoenent
void ACFH_PlayerCharacter::CallAnimAction()
{
	if (GI->IsSpeakActive == 1.f)
	{
		// Bool to say if okay to commence with animation
		bool ProceedWithAction = false;

		if (AnimInst->GetAnimStance() == EAnimStance::Aggressive)
		{
			ProceedWithAction = GI->CurrentlyActiveObject->PlayerActionRequest(EActionType::AggressiveAnim);
		}
		else if (AnimInst->GetAnimStance() == EAnimStance::Passive)
		{
			ProceedWithAction = GI->CurrentlyActiveObject->PlayerActionRequest(EActionType::PassiveAnim);
		}

		if (ProceedWithAction)
		{
			UAnimSequence* AnimationSequence = GI->CurrentlyActiveObject->ActiveAnimation;

			if (AnimationSequence) {
				// Get Duration to set timer to end animation
				float SeqDuration = (AnimationSequence->GetRawNumberOfFrames() / AnimationSequence->GetFrameRate());
				//UE_LOG(LogTemp, Warning, TEXT("ActionCalled: %f, %f"), AnimationMontage->GetSectionLength(0), SeqDuration);

				// Set Animation to be called
				ActionAnim->CreateAnimation(AnimationSequence);
				
				// Change EActionAnim to active so animation gets played
				AnimInst->StartActionAnim(SeqDuration);
				UE_LOG(LogTemp, Warning, TEXT("ActionCalled"));
			}
		}
	}
}

void ACFH_PlayerCharacter::PassToAudioInput()
{
	CallAudioInput();
}
// CHANGE: Move to CharacterAudioHandler Component
void ACFH_PlayerCharacter::CallAudioInput(bool ObjectRequest)
{
	if (GI->IsSpeakActive == 1.f)
	{
		// Check if it is a NPCSpeak sequence
		if (!ObjectRequest)
		{
			if (GI->CurrentlyActiveObject->PlayerActionRequest(EActionType::PlayerSpeak))
			{
				InteractiveObjectDialogueSpeaker->PlayObjectAudio(GI->CurrentlyActiveObject->ActiveDialogue, GI->CurrentlyActiveObject->ActiveAudioClip, GI->CurrentlyActiveObject->ActionOverride);
			}
		}
		else
		{
			InteractiveObjectDialogueSpeaker->PlayObjectAudio(GI->CurrentlyActiveObject->ActiveDialogue, GI->CurrentlyActiveObject->ActiveAudioClip, GI->CurrentlyActiveObject->ActionOverride);
			UE_LOG(LogTemp, Warning, TEXT("Called NPC Speak audio"))
		}
	}
}

void ACFH_PlayerCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// In not in animation process
		if (AnimInst->GetActionAnim() != EActionAnim::Active && !CharacterActionHandler->MoveForAnim)
		{
			// add movement in that direction
			AddMovementInput(GetActorForwardVector(), Value);
		}
	}
}

void ACFH_PlayerCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// In not in animation process
		if (AnimInst->GetActionAnim() != EActionAnim::Active && !CharacterActionHandler->MoveForAnim)
		{
			// add movement in that direction
			AddMovementInput(GetActorRightVector(), Value);
		}
	}
}

void ACFH_PlayerCharacter::TurnAtRate(float Rate)
{
	// Current rotation value based on rate input
	AddControllerYawInput(Rate * (BaseTurnRate)*GetWorld()->GetDeltaSeconds());
}

void ACFH_PlayerCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}