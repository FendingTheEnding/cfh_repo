// Kenneth Kratzer 2020


#include "ActionSequenceMonitor.h"
#include "ActionSequenceSetter.h"
#include "GameFramework/Actor.h"
#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UActionSequenceMonitor::UActionSequenceMonitor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UActionSequenceMonitor::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UActionSequenceMonitor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UActionSequenceMonitor::ActionMonitorSetup(int32* LocAggressiveAnimPtr, int32* LocPassiveAnimPtr, 
												int32* LocPlayerSpeakPtr, int32* LocPlayerAggroSpeakPtr, int32* LocPlayerPassSpeakPtr,
												int32* LocNPCSpeakPtr, int32*LocNPCAggroSpeakPtr, int32* LocNPCPassSpeakPtr)
{
	LocAggressiveAnim = LocAggressiveAnimPtr;
	LocPassiveAnim = LocPassiveAnimPtr;
	LocPlayerSpeak = LocPlayerSpeakPtr;
	LocPlayerAggroSpeak = LocPlayerAggroSpeakPtr;
	LocPlayerPassSpeak = LocPlayerPassSpeakPtr;
	LocNPCSpeak = LocNPCSpeakPtr;
	LocNPCAggroSpeak = LocNPCAggroSpeakPtr;
	LocNPCPassSpeak = LocNPCPassSpeakPtr;
}

bool UActionSequenceMonitor::PlayerActionRequest(EActionType RequestedAction, int32& ActiveSequenceNum, bool ObjectInViewport)
{
	// FIX: UE_LOG(LogTemp, Warning, TEXT("PlayerActionRequest Called!!"))
	// Have to be looking at object to perform animation
	if (ObjectInViewport)
	{
		if (*LocAggressiveAnim != -1 && RequestedAction == EActionType::AggressiveAnim)
		{
			ActiveSequenceNum = *LocAggressiveAnim;
			return true;
		}
		else if (*LocPassiveAnim != -1 && RequestedAction == EActionType::PassiveAnim)
		{
			ActiveSequenceNum = *LocPassiveAnim;
			return true;
		}
	}
	if (RequestedAction == EActionType::PlayerAggroSpeak)
	{
		if (*LocPlayerAggroSpeak != -1)
		{
			ActiveSequenceNum = *LocPlayerAggroSpeak;
			return true;
		}
		else if (*LocPlayerSpeak != -1)
		{
			ActiveSequenceNum = *LocPlayerSpeak;
			return true;
		}
		return false;
	}
	else if (RequestedAction == EActionType::PlayerPassSpeak)
	{
		if (*LocPlayerPassSpeak != -1)
		{
			ActiveSequenceNum = *LocPlayerPassSpeak;
			return true;
		}
		else if (*LocPlayerSpeak != -1)
		{
			ActiveSequenceNum = *LocPlayerSpeak;
			return true;
		}
		return false;
	}
	return false;
}

bool UActionSequenceMonitor::ObjectActionRequest(EActionType RequestedAction, int32& ActiveSequenceNum)
{
	// FIX: UE_LOG(LogTemp, Warning, TEXT("ObjectActionRequest Called!!"))
	if (RequestedAction == EActionType::NPCAggroSpeak)
	{
		if (*LocNPCAggroSpeak != -1)
		{
			ActiveSequenceNum = *LocNPCAggroSpeak;
			return true;
		}
		else if (*LocNPCSpeak != -1)
		{
			ActiveSequenceNum = *LocNPCSpeak;
			return true;
		}
		return false;
	}
	else if (RequestedAction == EActionType::NPCPassSpeak)
	{
		if (*LocNPCPassSpeak != -1)
		{
			ActiveSequenceNum = *LocNPCPassSpeak;
			return true;
		}
		else if (*LocNPCSpeak != -1)
		{
			ActiveSequenceNum = *LocNPCSpeak;
			return true;
		}
		return false;
	}
	// FIX: Add buffer requests
	return false;
}