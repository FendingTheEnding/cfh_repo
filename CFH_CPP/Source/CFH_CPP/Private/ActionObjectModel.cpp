// Kenneth Kratzer 2020


#include "ActionObjectModel.h"
#include "ActionGuideRetriever.h"
#include "ActionSequenceMonitor.h"
#include "ActionSequenceSetter.h"
#include "CFH_CharacterActionHandler.h"
#include "InViewportHandler.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"
#include "MyGameInstanceCpp.h"

// Sets default values
AActionObjectModel::AActionObjectModel()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Used as root component so meshes can be moved within blueprint
	SceneRoot = CreateDefaultSubobject<USceneComponent>(FName("SceneRoot"));
	SetRootComponent(SceneRoot);
	SceneRoot->SetVisibility(false);
	SceneRoot->SetMobility(EComponentMobility::Static);

	// Add all action guide components
	ActionGuideRetriever = CreateDefaultSubobject<UActionGuideRetriever>(FName("Action Guide Retriever"));
	ActionSequenceSetter = CreateDefaultSubobject<UActionSequenceSetter>(FName("Action Sequence Setter"));
	ActionSequenceMonitor = CreateDefaultSubobject<UActionSequenceMonitor>(FName("Action Sequence Monitor"));

}

// Called when the game starts or when spawned
void AActionObjectModel::BeginPlay()
{
	Super::BeginPlay();
	
	// Get required classes and validate that they aren't nullptr'd
	GI = Cast<UMyGameInstanceCpp>(GetWorld()->GetGameInstance());
	MyCharacter = Cast<ACharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	InViewportHandler = MyCharacter->FindComponentByClass<UInViewportHandler>();
	CharacterActionHandler = MyCharacter->FindComponentByClass<UCFH_CharacterActionHandler>();
	if (!GI || !MyCharacter || !InViewportHandler || !CharacterActionHandler)
	{
		UE_LOG(LogTemp, Error, TEXT("Check GameInstance MyCharacter InViewportHandler and InteractiveObjectDialogueSpeaker get loaded"));
	}
	else
	{
		// Get object's action guide
		FActionGuide* ActionGuide = ActionGuideRetriever->GetActionGuide(ActionGuideName, ActionSequenceSetter, GI);
		if (!ActionGuide)
		{
			UE_LOG(LogTemp, Error, TEXT("%s: Action Guide not loaded or InValid!!"), *ActionGuideName.ToString());
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Successful Action Guide Retrieval"))
			// Setup components with references to required variables
			ActionSequenceSetter->ActionSetterSetup(ActionGuide, &ActiveSequenceNum,
				&LocAggressiveAnim, &LocPassiveAnim, 
				&LocPlayerSpeak, &LocPlayerAggroSpeak, &LocPlayerPassSpeak,
				&LocNPCSpeak, &LocNPCAggroSpeak, &LocNPCPassSpeak,
				&ActiveDialogue, &ActiveAudioClip, &ActiveAnimation);
			ActionSequenceMonitor->ActionMonitorSetup(&LocAggressiveAnim, &LocPassiveAnim, 
				&LocPlayerSpeak, &LocPlayerAggroSpeak, &LocPlayerPassSpeak,
				&LocNPCSpeak, &LocNPCAggroSpeak, &LocNPCPassSpeak);
			UE_LOG(LogTemp, Warning, TEXT("Object Initialized: %s"), *ActionGuideName.ToString());

			// Set so object can now act
			EndAction = false;
		}
	}
}

// Called every frame
void AActionObjectModel::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FVector AActionObjectModel::GetAnimationPositioning()
{
	FVector EndLoc = { 0, 0, 0 };
	FVector ObjLoc = GetActorLocation();
	FVector PlayerLoc = MyCharacter->GetActorLocation();
	for (int32 i = 0; i < 3; i++)
	{
		// Direction in line with object
		if (CharacterPositioning[i] == 1)
		{
			EndLoc[i] = ObjLoc[i];
		}
		// Keep Z direction so height doesn't change
		else if (CharacterPositioning[i] == 0)
		{
			EndLoc[i] = PlayerLoc[i];
		}
		// Set distance character is from object
		else
		{
			EndLoc[i] = CharacterPositioning[i];
		}
	}

	return EndLoc;
}

bool AActionObjectModel::PlayerActionRequest(EActionType RequestedAction)
{
	if (ActionSequenceMonitor->PlayerActionRequest(RequestedAction, ActiveSequenceNum, InViewportHandler->ObjectInViewport(GetActorLocation())))
	{
		PreActionPrep();
		return true;
	}
	return false;
}

bool AActionObjectModel::ObjectActionRequest(EActionType RequestedAction)
{
	if (ActionSequenceMonitor->ObjectActionRequest(RequestedAction, ActiveSequenceNum))
	{
		PreActionPrep();
		return true;
	}
	return false;
}

void AActionObjectModel::PerformOnAnim()
{
	ActionSequenceSetter->PrepSequenceActive(ActionBufferTime);
}

void AActionObjectModel::PreActionPrep()
{
	ActionSequenceSetter->SetActiveSequenceValues(ActionBufferTime);
}

void AActionObjectModel::SetRenderDepth(bool bValue) const
{
	// Used by sub-classes to set render depth 
	// Passed to sub-class so they can decide if used on static mesh, or skeletal mesh, or etc.
}