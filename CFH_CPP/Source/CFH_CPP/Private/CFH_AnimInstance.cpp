// Kenneth Kratzer 2020


#include "CFH_AnimInstance.h"
#include "GameFramework/Character.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"


UCFH_AnimInstance::UCFH_AnimInstance()
{

}

// Called when the game starts
void UCFH_AnimInstance::NativeBeginPlay()
{
	SetupInputComponent();
}

//Tick
void UCFH_AnimInstance::NativeUpdateAnimation(float DeltaTimeX)
{
	//Very Important Line
	Super::NativeUpdateAnimation(DeltaTimeX);

	// Set for use in Blend Space
	CharVel = GetOwningActor()->GetVelocity();
	CharRot = GetOwningActor()->GetActorRotation();
}

void UCFH_AnimInstance::SetupInputComponent()
{
	UInputComponent* InputComponent = Cast<ACharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0))->FindComponentByClass<UInputComponent>();
	if (InputComponent)
	{
		InputComponent->BindAction("StanceChange", IE_Pressed, this, &UCFH_AnimInstance::SetAnimStance);
	}
}

EAnimStance UCFH_AnimInstance::GetAnimStance() const
{
	return AnimStance;
}

void UCFH_AnimInstance::SetAnimStance()
{
	if (AnimStance == EAnimStance::Aggressive) {
		AnimStance = EAnimStance::Passive;
		UE_LOG(LogTemp, Warning, TEXT("Set Passive"));
	}
	else if (AnimStance == EAnimStance::Passive) {
		AnimStance = EAnimStance::Aggressive;
		UE_LOG(LogTemp, Warning, TEXT("Set Aggressive"));
	}
}

EActionAnim UCFH_AnimInstance::GetActionAnim() const
{
	//UE_LOG(LogTemp, Warning, TEXT("Called"));
	return ActionAnim;
}

void UCFH_AnimInstance::StartActionAnim(float SequenceDuration)
{
	// Make sure an animation isn't currently in effect
	if (ActionAnim == EActionAnim::Inactive) {
		// Set animation into effect
		ActionAnim = EActionAnim::Active;
		UE_LOG(LogTemp, Warning, TEXT("Acting"));
		FTimerHandle AnimThandle;
		// Set timer to stop animation
		GetWorld()->GetTimerManager().SetTimer(AnimThandle, this, &UCFH_AnimInstance::EndActionAnim, SequenceDuration, false);
	}
}

void UCFH_AnimInstance::EndActionAnim()
{
	// Check that animation is currently in effect
	if (ActionAnim == EActionAnim::Active) {
		// De-activate animation
		ActionAnim = EActionAnim::Inactive;
		UE_LOG(LogTemp, Warning, TEXT("Inactive"));
	}
}

ETurnAnim UCFH_AnimInstance::GetTurnAnim() const
{
	//UE_LOG(LogTemp, Warning, TEXT("Called"));
	return TurnAnim;
}

void UCFH_AnimInstance::SetTurnAnim(int32 State)
{
	if (State == 0) {
		TurnAnim = ETurnAnim::Left;
	}
	else if (State == 1) {
		TurnAnim = ETurnAnim::Right;
	}
	else {
		TurnAnim = ETurnAnim::Inactive;
	}
}
