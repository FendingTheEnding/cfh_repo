// Kenneth Kratzer 2020


#include "ActionSequenceSetter.h"
#include "ActionObjectModel.h"
#include "MyGameInstanceCpp.h"

// Sets default values for this component's properties
UActionSequenceSetter::UActionSequenceSetter()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UActionSequenceSetter::BeginPlay()
{
	Super::BeginPlay();

	// ...
	GI = Cast<UMyGameInstanceCpp>(GetWorld()->GetGameInstance());
}

// Called every frame
void UActionSequenceSetter::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UActionSequenceSetter::ActionSetterSetup(FActionGuide* ActionGuide, int32* ActiveSequenceNumPtr,
										int32* LocAggressiveAnimPtr, int32* LocPassiveAnimPtr, 
										int32* LocPlayerSpeakPtr, int32* LocPlayerAggroSpeakPtr, int32* LocPlayerPassSpeakPtr, 
										int32* LocNPCSpeakPtr, int32* LocNPCAggroSpeakPtr, int32* LocNPCPassSpeakPtr,
										FString* ActiveDialoguePtr, USoundWave** ActiveAudioClipPtr, UAnimSequence** ActiveAnimationPtr)
{
	HexIdentifier = FParse::HexNumber(*ActionGuide->ReferenceNumber);;
	ActionSequence = ActionGuide->ActionSequence;
	CurrentSequence = ActionGuide->CurSequence;
	NextSequence = ActionGuide->NextSequence;
	ActionSequence = ActionGuide->ActionSequence;
	Dialogue = ActionGuide->Dialogue;
	AudioClips = ActionGuide->AudioClips;
	AnimationReferences = ActionGuide->AnimRefs;

	ActiveSequenceNum = ActiveSequenceNumPtr;

	LocAggressiveAnim = LocAggressiveAnimPtr;
	LocPassiveAnim = LocPassiveAnimPtr;
	LocPlayerSpeak = LocPlayerSpeakPtr;
	LocPlayerAggroSpeak = LocPlayerAggroSpeakPtr;
	LocPlayerPassSpeak = LocPlayerPassSpeakPtr;
	LocNPCSpeak = LocNPCSpeakPtr;
	LocNPCAggroSpeak = LocNPCAggroSpeakPtr;
	LocNPCPassSpeak = LocNPCPassSpeakPtr;

	ActiveDialogue = ActiveDialoguePtr;
	ActiveAudioClip = ActiveAudioClipPtr;
	ActiveAnimation = ActiveAnimationPtr;

	// Set inc to determine how many variables there are before '-8888.f', which is used to signal start of next sequence
	while (CurrentSequence[CurSeqCnt] != -8888.f)
	{
		CurSeqCnt++;
	}
	// Account for sentinel value of -8888.f
	CurSeqCnt++;

	// Needed to start component
	SetNextSequenceOptions();
}

void UActionSequenceSetter::SetSequenceIncs()
{
	CurInc = 0;
	NextInc = 0;
	int i = 0;

	while (i != *ActiveSequenceNum)
	{
		// Increment based on how many Current Sequence variables there are
		CurInc += CurSeqCnt;
		// Set based on End values to get through each sequence section
		while (NextSequence[NextInc] != FString("End"))
		{
			NextInc++;
		}
		// To move off the "End" for next loop
		NextInc++;
		i++;
	}
}

void UActionSequenceSetter::SetActiveSequenceValues(float ActionBufferTime)
{
	if (!SequenceActive)
	{
		// There was one more to add... what was it!?!?
		// Some kind of OVERRIDE... idk what for
		// FIX: Left for debug
		UE_LOG(LogTemp, Warning, TEXT("SetActiveSequenceValues Called"));

		// Set true so to lock out repeated calls
		SequenceActive = true;

		// Set increments based on selected active sequence
		SetSequenceIncs();

		// Loop though number of variables in Current Sequence
		for (int i = 0; i < CurSeqCnt - 1; i++)
		{
			switch (i)
			{
			default:
				break;
			case 0:
				SelectionInc = CurrentSequence[CurInc];
			case 1:
				ActionOverride = (bool)CurrentSequence[CurInc];
			case 2:
				Delay = CurrentSequence[CurInc];
			case 3:
				ObjAnim = CurrentSequence[CurInc];
			case 4:
				BufferNum = CurrentSequence[CurInc];
			case 5:
				BufferDelay = CurrentSequence[CurInc];
			case 6:
				BufferRetarget = CurrentSequence[CurInc];
			case 7:
				ScannableObject = (bool)CurrentSequence[CurInc];
			case 8:
				ObjectMarker = (bool)CurrentSequence[CurInc];
				// FIX: For debug
				ObjectMarker = true;
			}
			CurInc++;
		}
		// Set for audio call
		if (ActionSequence[*ActiveSequenceNum] == EActionType::PlayerSpeak || ActionSequence[*ActiveSequenceNum] == EActionType::NPCSpeak)
		{
			*ActiveDialogue = Dialogue[SelectionInc];
			*ActiveAudioClip = AudioClips[SelectionInc];
			WaitDuration = AudioClips[SelectionInc]->GetDuration();
			// FIX: For Debug
			UE_LOG(LogTemp, Warning, TEXT("Audio Action Set"))
			// Set to < -1 for NPCSpeak to start ObjectActionRequest
			if (ActionSequence[*ActiveSequenceNum] == EActionType::PlayerSpeak)
			{
				*ActiveSequenceNum = -1;
				PrepSequenceActive(ActionBufferTime);
			}
			else if (ActionSequence[*ActiveSequenceNum] == EActionType::NPCSpeak)
			{
				*ActiveSequenceNum = -((int32)ActionSequence[*ActiveSequenceNum]);
				PrepSequenceActive(ActionBufferTime);
			}

		}
		// Set for animation call
		else if (ActionSequence[*ActiveSequenceNum] == EActionType::AggressiveAnim || ActionSequence[*ActiveSequenceNum] == EActionType::PassiveAnim)
		{
			*ActiveAnimation = AnimationReferences[SelectionInc];
			WaitDuration = (AnimationReferences[SelectionInc]->GetRawNumberOfFrames() / AnimationReferences[SelectionInc]->GetFrameRate());
			// FIX: For Debug
			UE_LOG(LogTemp, Warning, TEXT("Anim Action Set"));
			*ActiveSequenceNum = -1;
			if (ObjAnim > -1)
			{
				ObjectAnimation = AnimationReferences[ObjAnim];
			}
			else
			{
				ObjectAnimation = nullptr;
			}
		}
	}
}

void UActionSequenceSetter::PrepSequenceActive(float ActionBufferTime)
{
	// Set timer based on time action takes to process
	// Fix: Add possible delay time additions
	FTimerHandle SeqThandle;
	GetWorld()->GetTimerManager().SetTimer(SeqThandle, this, &UActionSequenceSetter::SetSequenceActive, WaitDuration + ActionBufferTime, false);
	UE_LOG(LogTemp, Error, TEXT("Timer Set: %f"), WaitDuration + ActionBufferTime);
}

// NEEDS TO BE NO INPUTS
void UActionSequenceSetter::SetSequenceActive()
{
	// Move to set up for next sequence
	SequenceActive = false;
	UE_LOG(LogTemp, Warning, TEXT("SettingNext"))
	SetNextSequenceOptions();
}

// bool true if action to perform was assigned or false if we need to wait for user input?
// Figure out how to signal NPC speak in owner; Maybe via above comment
void UActionSequenceSetter::SetNextSequenceOptions()
{
	UE_LOG(LogTemp, Warning, TEXT("SetNextSequenceOptions Called"))
	// Reset for if retarget occurs
	RetargetAssignment = false;

	// Reset sequence location values of each player wait action type
	*LocAggressiveAnim = -1;
	*LocPassiveAnim = -1;
	*LocPlayerSpeak = -1;
	*LocPlayerAggroSpeak = -1;
	*LocPlayerPassSpeak = -1;
	*LocNPCSpeak = -1;
	*LocNPCAggroSpeak = -1;
	*LocNPCPassSpeak = -1;
	//SpeakLocArr = {};
	//SpeakStanceArr = {};

	// Loop though NextSequence section activating each possible type
	while (NextSequence[NextInc] != FString("End"))
	{
		/*
		FString StanceLockType = TEXT("Nope");
		if (!SequenceCheck.RightChop(1).IsNumeric())
		{
			StanceLockType = SequenceCheck.RightChop(1);
			SequenceCheck = SequenceCheck.Right(1);
		}
		*/
		FString SequenceCheck = NextSequence[NextInc];
		int32 HexRequest = FParse::HexNumber(*SequenceCheck.Left(3));
		int32 SeqLoc = FCString::Atoi(*SequenceCheck.RightChop(3));
		UE_LOG(LogTemp, Warning, TEXT("Sequence: %s, %d, %d"), *SequenceCheck, HexRequest, SeqLoc)
		// Check that call is for this object
		if (HexRequest == HexIdentifier)
		{
			if (ActionSequence[SeqLoc] == EActionType::AggressiveAnim)
			{
				*LocAggressiveAnim = SeqLoc;
				//UE_LOG(LogTemp, Warning, TEXT("AgroAnim"));
			}
			else if (ActionSequence[SeqLoc] == EActionType::PassiveAnim)
			{
				*LocPassiveAnim = SeqLoc;
				//UE_LOG(LogTemp, Warning, TEXT("PassAnim"));
			}
			else if (ActionSequence[SeqLoc] == EActionType::PlayerSpeak)
			{
				*LocPlayerSpeak = SeqLoc;
				UE_LOG(LogTemp, Warning, TEXT("SetNextPlayer: %d, %d"), *LocPlayerSpeak, SeqLoc)
			}
			else if (ActionSequence[SeqLoc] == EActionType::PlayerAggroSpeak)
			{
				*LocPlayerAggroSpeak = SeqLoc;
			}
			else if (ActionSequence[SeqLoc] == EActionType::PlayerPassSpeak)
			{
				*LocPlayerPassSpeak = SeqLoc;
			}
			else if (ActionSequence[SeqLoc] == EActionType::NPCSpeak)
			{
				*LocNPCSpeak = SeqLoc;
				*ActiveSequenceNum = SeqLoc;
				UE_LOG(LogTemp, Warning, TEXT("SetNextNPC: %d, %d"), *LocNPCSpeak, SeqLoc)
			}
			else if (ActionSequence[SeqLoc] == EActionType::NPCAggroSpeak)
			{
				*LocNPCAggroSpeak = SeqLoc;
				*ActiveSequenceNum = SeqLoc;
			}
			else if (ActionSequence[SeqLoc] == EActionType::NPCPassSpeak)
			{
				*LocNPCPassSpeak = SeqLoc;
				*ActiveSequenceNum = SeqLoc;
			}
			else if (ActionSequence[SeqLoc] == EActionType::EndStage)
			{
				*ActiveSequenceNum = -8888;
				// Deactivate object for remainder of game
				/*
				GoForSpeak = 0.f;
				GI->SetSpeak(0.f);
				VisibilityState = EWidgetVisibility::Unseen;
				UE_LOG(LogTemp, Warning, TEXT("%s: Deactivated"), *StaticMeshName.ToString());
				*/
			}
		}
		else
		{
			// Call to external object through reference arrays
			int32 RefLoc = GI->ReferenceNumArr.Find(HexRequest);
			GI->ActionObjectArray[RefLoc]->RetargetCompAssignment(NextSequence[NextInc]);
		}
		// Increment to next selection in NextSequence array
		NextInc++;
	}
}

void UActionSequenceSetter::RetargetCompAssignment(FString RetargetRef)
{
	// Reset variables so that retarget completely redoes NextSequence setup
	if (!RetargetAssignment)
	{
		RetargetAssignment = true;
		*LocAggressiveAnim = -1;
		*LocPassiveAnim = -1;
		*LocPlayerSpeak = -1;
		*LocPlayerAggroSpeak = -1;
		*LocPlayerPassSpeak = -1;
		*LocNPCSpeak = -1;
		*LocNPCAggroSpeak = -1;
		*LocNPCPassSpeak = -1;
	}
	int32 HexRequest = FParse::HexNumber(*RetargetRef.Left(3));
	int32 SeqLoc = FCString::Atoi(*RetargetRef.RightChop(3));
	if (HexRequest == HexIdentifier)
	{
		if (ActionSequence[SeqLoc] == EActionType::AggressiveAnim)
		{
			*LocAggressiveAnim = SeqLoc;
			//UE_LOG(LogTemp, Warning, TEXT("EX: AgroAnim"));
		}
		else if (ActionSequence[SeqLoc] == EActionType::PassiveAnim)
		{
			*LocPassiveAnim = SeqLoc;
			//UE_LOG(LogTemp, Warning, TEXT("EX: PassAnim"));
		}
		else if (ActionSequence[SeqLoc] == EActionType::PlayerSpeak)
		{
			*LocPlayerSpeak = SeqLoc;
			//UE_LOG(LogTemp, Warning, TEXT("EX: MC Speak"));
		}
		else if (ActionSequence[SeqLoc] == EActionType::PlayerAggroSpeak)
		{
			*LocPlayerAggroSpeak = SeqLoc;
		}
		else if (ActionSequence[SeqLoc] == EActionType::PlayerPassSpeak)
		{
			*LocPlayerPassSpeak = SeqLoc;
		}
		else if (ActionSequence[SeqLoc] == EActionType::NPCSpeak)
		{
			*LocNPCSpeak = SeqLoc;
			//UE_LOG(LogTemp, Warning, TEXT("EX: NPCSpeak"));
		}
		else if (ActionSequence[SeqLoc] == EActionType::NPCAggroSpeak)
		{
			*LocNPCAggroSpeak = SeqLoc;
		}
		else if (ActionSequence[SeqLoc] == EActionType::NPCPassSpeak)
		{
			*LocNPCPassSpeak = SeqLoc;
		}
		else if (ActionSequence[SeqLoc] == EActionType::EndStage)
		{
			/*
			GoForSpeak = 0.f;
			GI->SetSpeak(0.f);
			VisibilityState = EWidgetVisibility::Unseen;
			UE_LOG(LogTemp, Warning, TEXT("%s: Deactivated"), *StaticMeshName.ToString())
			*/
		}
	}

}

UAnimationAsset* UActionSequenceSetter::GetObjectAnimation()
{
	return ObjectAnimation;
}