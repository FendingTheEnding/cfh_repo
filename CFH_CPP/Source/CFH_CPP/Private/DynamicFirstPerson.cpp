// Kenneth Kratzer 2020


#include "DynamicFirstPerson.h"
#include "Camera/CameraComponent.h"
#include "CFH_AnimInstance.h"
#include "CFH_CharacterActionHandler.h"
#include "GameFramework/Character.h"
#include "MyGameInstanceCpp.h"
//#include "GameFramework/PlayerController.h"
//#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UDynamicFirstPerson::UDynamicFirstPerson()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UDynamicFirstPerson::BeginPlay()
{
	Super::BeginPlay();

	// ...

	//MyController = Cast<APlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	GI = Cast<UMyGameInstanceCpp>(GetWorld()->GetGameInstance());
	MyCharacter = Cast<ACharacter>(GetOwner());
	AnimInstance = Cast<UCFH_AnimInstance>(MyCharacter->GetMesh()->GetAnimInstance());

	InitialRotationDifference = MyCharacter->GetMesh()->GetComponentRotation().Yaw - MyCharacter->FindComponentByClass<UCameraComponent>()->GetComponentRotation().Yaw;
	if (InitialRotationDifference < 0)
	{
		InitialRotationDifference += 360;
	}
	SetupInputComponent();
}


// Called every frame
void UDynamicFirstPerson::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UDynamicFirstPerson::SetupInputComponent()
{
	UInputComponent* InputComponent = GetOwner()->FindComponentByClass<UInputComponent>();
	for (FName BindAxis : AxisToBind)
	{
		for (int32 Inc = 0; Inc < InputComponent->AxisBindings.Num(); Inc++)
		{
			if (InputComponent->AxisBindings[Inc].AxisName == BindAxis)
			{
				InputComponent->AxisBindings.RemoveAt(Inc);
			}
		}
	}
	/*
	*/
	InputComponent->BindAxis("Turn", this, &UDynamicFirstPerson::TurnAtRate);
	InputComponent->BindAxis("TurnRate", this, &UDynamicFirstPerson::TurnAtRate);
	InputComponent->BindAxis("LookUp", this, &UDynamicFirstPerson::LookUpAtRate);
	InputComponent->BindAxis("LookUpRate", this, &UDynamicFirstPerson::LookUpAtRate);

	UE_LOG(LogTemp, Warning, TEXT("Axis Re-Bound"))
}

void UDynamicFirstPerson::TurnAtRate(float Rate)
{
	// Find rotation diff between camera and mesh; initially 270 deg
	
	float RotDif = MyCharacter->GetMesh()->GetComponentRotation().Yaw - MyCharacter->FindComponentByClass<UCameraComponent>()->GetComponentRotation().Yaw;
	float Speed = MyCharacter->GetVelocity().Size();
	// Current rotation value based on rate input
	float RotVal = Rate * (BaseTurnRate)*GetWorld()->GetDeltaSeconds();
	// Used to measure viewport yaw movement
	DegMoved += RotVal;

	// If in an animation
	if (AnimInstance->GetActionAnim() == EActionAnim::Active)
	{
		float NewYaw = MyCharacter->GetControlRotation().Yaw + RotVal;
		
		//UE_LOG(LogTemp, Warning, TEXT("NewYaw: %f, AnimYawMax: %f, AnimYawMin: %f"), NewYaw, AnimYawMax, AnimYawMin)
		if (NewYaw < AnimYawMax && NewYaw > AnimYawMin)
		{
			MyCharacter->AddControllerYawInput(RotVal);
		}
		else
		{
			// rewire from neg/pos to increase/decrease use CharRot+Offset
			if (!AnimMaxFlipped && !AnimMinFlipped)
			{
				if (NewYaw < AnimYawMin && RotVal > 0)
				{
					MyCharacter->AddControllerYawInput(RotVal);
				}
				else if (NewYaw > AnimYawMax && RotVal < 0)
				{
					MyCharacter->AddControllerYawInput(RotVal);
				}
			}
			else
			{
				//UE_LOG(LogTemp, Warning, TEXT("%f"), RotVal);
				if (MyCharacter->GetControlRotation().Yaw > 180)
				{
					if (NewYaw > AnimYawMin && RotVal < 0)
					{
						MyCharacter->AddControllerYawInput(RotVal);
					}
					else if (RotVal > 0)
					{
						MyCharacter->AddControllerYawInput(RotVal);
					}
				}
				else if (MyCharacter->GetControlRotation().Yaw < 180)
				{
					if (NewYaw < AnimYawMax && RotVal > 0)
					{
						MyCharacter->AddControllerYawInput(RotVal);
					}
					else if (RotVal < 0)
					{
						MyCharacter->AddControllerYawInput(RotVal);
					}
				}
			}
		}
		MyCharacter->GetMesh()->SetWorldLocation(AnimLockedCharacterLocation);
		MyCharacter->GetMesh()->SetWorldRotation(AnimLockedCharacterRotation);
	}
	// If character is not in animation and not moving for animation
	else if (!Cast<UCFH_CharacterActionHandler>(MyCharacter->FindComponentByClass<UCFH_CharacterActionHandler>())->MoveForAnim)
	{
		// calculate delta for this frame from the rate information
		MyCharacter->AddControllerYawInput(RotVal);
		if (RotDif < 0) {
			RotDif += 360;
		}
		if (abs(DegMoved) > 25.f || Speed != 0) {
			if (RotDif > InitialRotationDifference + 0.5f) {
				MyCharacter->GetMesh()->AddRelativeRotation({ 0,-abs(2.f * (0.35f * (BaseTurnRate)*GetWorld()->GetDeltaSeconds())),0 });
				//AnimInst->SetTurnAnim(0);
			}
			else if (RotDif < InitialRotationDifference - 0.5f) {
				MyCharacter->GetMesh()->AddRelativeRotation({ 0,abs(2.f * (0.35f * (BaseTurnRate)*GetWorld()->GetDeltaSeconds())),0 });
				//AnimInst->SetTurnAnim(1);
			}
			else {
				DegMoved = 0;
				//AnimInst->SetTurnAnim();
			}
		}
		else {
			if (RotDif < InitialRotationDifference + 25.5 && RotDif > InitialRotationDifference - 25.5) {
				MyCharacter->GetMesh()->AddRelativeRotation({ 0, -RotVal, 0 });
			}
		}
	}
}

void UDynamicFirstPerson::LookUpAtRate(float Rate)
{

	// calculate delta for this frame from the rate information
	float RotVal = Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds();

	// If in an animation
	if (AnimInstance->GetActionAnim() == EActionAnim::Active)
	{
		float NewPitch = MyCharacter->GetControlRotation().GetNormalized().Pitch - RotVal;
		if (NewPitch < AnimPitchMax && NewPitch > AnimPitchMin)
		{
			MyCharacter->AddControllerPitchInput(RotVal);
		}
		else
		{
			// rewire from neg/pos to increase/decrease use CharRot+Offset
			if (NewPitch < AnimPitchMin + 20.f && RotVal < 0)
			{
				MyCharacter->AddControllerPitchInput(RotVal);
			}
			else if (NewPitch > AnimPitchMax - 20.f && RotVal > 0)
			{
				MyCharacter->AddControllerPitchInput(RotVal);
			}
		}
	}
	// If character is not in animation and not moving for animation
	else if (!Cast<UCFH_CharacterActionHandler>(MyCharacter->FindComponentByClass<UCFH_CharacterActionHandler>())->MoveForAnim) {
		MyCharacter->AddControllerPitchInput(RotVal);
	}
}

void UDynamicFirstPerson::SetAnimationLookValues(FVector AnimationCharacterLocation, FRotator AnimationCharacterRotation, float IntendedCameraYaw, float IntendedCameraPitch, float YawOffset, float PitchOffset)
{
	// Set Pitch and Yaw max and mins based on interacting object
	// FIX: Maybe don't need middle as persisting variable
	AnimLockedCharacterLocation = AnimationCharacterLocation;
	AnimLockedCharacterRotation = AnimationCharacterRotation;
	AnimYawMax = IntendedCameraYaw + YawOffset;
	AnimYawMin = IntendedCameraYaw - YawOffset;
	AnimPitchMax = IntendedCameraPitch + PitchOffset;
	AnimPitchMin = IntendedCameraPitch - PitchOffset;
	AnimMaxFlipped = false;
	AnimMinFlipped = false;

	if (AnimYawMax > 360 || AnimYawMin < 0)
	{
		if (AnimYawMax > 360)
		{
			AnimYawMax += -360;
			AnimMaxFlipped = true;
		}
		else if (AnimYawMin < 0)
		{
			AnimYawMin += 360;
			AnimMinFlipped = true;
		}
	}
}
