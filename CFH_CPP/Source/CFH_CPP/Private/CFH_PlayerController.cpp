// Kenneth Kratzer 2020


#include "CFH_PlayerController.h"

bool ACFH_PlayerController::GetSightRayHitLocation(int i, FVector& CameraTraceLocation, FRotator& CameraTraceRotation) const
{
	// Find the crosshair position
	int32 ViewportSizeX, ViewportSizeY;
	APlayerController::GetViewportSize(ViewportSizeX, ViewportSizeY);
	FVector2D ScreenLocation = FVector2D(ViewportSizeX*TraceLocationX[i], ViewportSizeY*TraceLocationY[i]);
	FVector CameraTraceDirection;
	if (GetLookDirection(ScreenLocation, CameraTraceLocation, CameraTraceDirection))
	{
		CameraTraceRotation = CameraTraceDirection.Rotation();
		return true;
	}
	return false;
}

bool ACFH_PlayerController::GetLookDirection(FVector2D ScreenLocation, FVector& CameraTraceLocation, FVector& CameraTraceDirection) const
{
	return DeprojectScreenPositionToWorld(ScreenLocation.X, ScreenLocation.Y, CameraTraceLocation, CameraTraceDirection);
}
