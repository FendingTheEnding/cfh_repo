// Kenneth Kratzer 2020


#include "StaticStandardActionObject.h"

AStaticStandardActionObject::AStaticStandardActionObject()
{
	StaticMeshObject = CreateDefaultSubobject<UStaticMeshComponent>(FName("Static Mesh Object"));
	StaticMeshObject->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

void AStaticStandardActionObject::BeginPlay()
{
	ActionGuideName = FName(*StaticMeshObject->GetStaticMesh()->GetName());
	Super::BeginPlay();
}

void AStaticStandardActionObject::SetRenderDepth(bool bValue) const
{
	StaticMeshObject->SetRenderCustomDepth(bValue);
}