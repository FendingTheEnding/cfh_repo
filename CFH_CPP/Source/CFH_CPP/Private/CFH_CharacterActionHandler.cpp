// Kenneth Kratzer 2020


#include "CFH_CharacterActionHandler.h"
#include "ActionObjectModel.h"
#include "CFH_AnimInstance.h"
#include "DynamicFirstPerson.h"
#include "GameFramework/Character.h"
#include "InteractiveObjectDialogueSpeaker.h"
#include "Kismet/GameplayStatics.h"
#include "MyGameInstanceCpp.h"

// Sets default values for this component's properties
UCFH_CharacterActionHandler::UCFH_CharacterActionHandler()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UCFH_CharacterActionHandler::BeginPlay()
{
	Super::BeginPlay();

	// ...
	// Assign required objects and check none are nullptred
	GI = Cast<UMyGameInstanceCpp>(GetWorld()->GetGameInstance());
	MyCharacter = Cast<ACharacter>(GetOwner());
	if (GI && MyCharacter && ActionAnim)
	{
		AnimInstance = Cast<UCFH_AnimInstance>(MyCharacter->GetMesh()->GetAnimInstance());
		CharacterSpeaker = Cast<UInteractiveObjectDialogueSpeaker>(MyCharacter->FindComponentByClass<UInteractiveObjectDialogueSpeaker>());
		if (!AnimInstance || !CharacterSpeaker)
		{
			UE_LOG(LogTemp, Error, TEXT("CharacterActionHandler failure to load AnimInstance or CharacterSpeaker from MyCharacter"))
		}
		// Assign action inputs
		SetupInputComponent();
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("CharacterActionHandler failure to load GI, MyCharacter, or ActionAnim; ActionAnim has to be loaded in blueprint."))
	}

}


// Called every frame
void UCFH_CharacterActionHandler::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...

	// Used to interpolate a move in position in prep for object oriented animation
		// CHANGE: Move to PlayerCharacter AnimationHandler?
	if (MoveForAnim)
	{
		// CharacterRotation set in editor as rotation for animation to start at
		MyCharacter->Controller->SetControlRotation(FMath::RInterpTo(MyCharacter->Controller->GetControlRotation(), GI->CurrentlyActiveObject->CharacterRotation, GetWorld()->GetDeltaSeconds(), 3.f));
		// AnimationLocation set in SetAnimationPositioning();
		MyCharacter->SetActorLocation(FMath::VInterpTo(MyCharacter->GetActorLocation(), AnimationLocation, GetWorld()->GetDeltaSeconds(), 3.f));
		//UE_LOG(LogTemp, Warning, TEXT("Working: %f, %f"), MyCharacter->GetActorLocation().Y, EndLoc.Y)
		if (abs(MyCharacter->GetActorLocation().Y) > abs(AnimationLocation.Y) - 0.25f && abs(MyCharacter->GetActorLocation().Y) < abs(AnimationLocation.Y) + 0.25f)
		{
			FVector AnimationCharacterLocation = AnimationLocation + MyCharacter->GetMesh()->GetRelativeLocation();
			FRotator AnimationCharacterRotation = MyCharacter->GetMesh()->GetRelativeRotation() + FRotator({ 0, GI->CurrentlyActiveObject->CharacterRotation.Yaw, 0 }); 
			Cast<UDynamicFirstPerson>(MyCharacter->FindComponentByClass<UDynamicFirstPerson>())->SetAnimationLookValues(
				AnimationCharacterLocation, AnimationCharacterRotation, GI->CurrentlyActiveObject->CharacterRotation.Yaw, 
				GI->CurrentlyActiveObject->CharacterRotation.Pitch, GI->CurrentlyActiveObject->CharacterRotationOffset.Yaw, GI->CurrentlyActiveObject->CharacterRotationOffset.Pitch
			);
			MoveForAnim = false;
			PerformAnimation();
			GI->CurrentlyActiveObject->PerformOnAnim();
		}
	}
}

void UCFH_CharacterActionHandler::SetupInputComponent()
{
	// Get from Player's Character
	UInputComponent* InputComponent = Cast<ACharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0))->FindComponentByClass<UInputComponent>();
	if (InputComponent)
	{
		// Bind actions
		InputComponent->BindAction("AudioInteract", IE_Pressed, this, &UCFH_CharacterActionHandler::PassToAudioInput);
		InputComponent->BindAction("PhysicalInteract", IE_Pressed, this, &UCFH_CharacterActionHandler::CallAnimAction);
	}
}

bool UCFH_CharacterActionHandler::ProceedWithAction(FString ActionType) const
{
	UE_LOG(LogTemp, Warning, TEXT("ProceedWithAction Called"))
	if (ActionType == FString("Animation"))
	{
		if (AnimInstance->GetAnimStance() == EAnimStance::Aggressive)
		{
			return GI->CurrentlyActiveObject->PlayerActionRequest(EActionType::AggressiveAnim);
		}
		else if (AnimInstance->GetAnimStance() == EAnimStance::Passive)
		{
			return GI->CurrentlyActiveObject->PlayerActionRequest(EActionType::PassiveAnim);
		}
	}
	else if (ActionType == FString("Audio"))
	{
		if (AnimInstance->GetAnimStance() == EAnimStance::Aggressive)
		{
			return GI->CurrentlyActiveObject->PlayerActionRequest(EActionType::PlayerAggroSpeak);
		}
		else if (AnimInstance->GetAnimStance() == EAnimStance::Passive)
		{
			return GI->CurrentlyActiveObject->PlayerActionRequest(EActionType::PlayerPassSpeak);
		}
	}
	else if (ActionType == FString("Object"))
	{

		if (AnimInstance->GetAnimStance() == EAnimStance::Aggressive)
		{
			return GI->CurrentlyActiveObject->ObjectActionRequest(EActionType::NPCAggroSpeak);
		}
		else if (AnimInstance->GetAnimStance() == EAnimStance::Passive)
		{
			return GI->CurrentlyActiveObject->ObjectActionRequest(EActionType::NPCPassSpeak);
		}
	}
	return false;
}

void UCFH_CharacterActionHandler::CallAnimAction()
{
	if (GI->IsSpeakActive == 1.f && !MoveForAnim)
	{

		if (ProceedWithAction(FString("Animation")))
		{
			AnimationLocation = GI->CurrentlyActiveObject->GetAnimationPositioning();
			MoveForAnim = true;
		}
	}
}

void UCFH_CharacterActionHandler::PerformAnimation()
{
	// Active Animation is assigned when ProceedWithAction is returned true
	UAnimSequence* AnimationSequence = GI->CurrentlyActiveObject->ActiveAnimation;

	if (AnimationSequence) {
		// Get Duration to set timer to end animation
		float SeqDuration = (AnimationSequence->GetRawNumberOfFrames() / AnimationSequence->GetFrameRate());
		//UE_LOG(LogTemp, Warning, TEXT("ActionCalled: %f, %f"), AnimationMontage->GetSectionLength(0), SeqDuration);

		// Set Animation to be called
		ActionAnim->CreateAnimation(AnimationSequence);

		// Change EActionAnim to active so animation gets played; SeqDuration passed so AnimInstance knows when to make inactive
		AnimInstance->StartActionAnim(SeqDuration);
		UE_LOG(LogTemp, Warning, TEXT("ActionCalled"));
	}
}

void UCFH_CharacterActionHandler::PassToAudioInput()
{
	// False to say its a PlayerRequest and not an NPCRequest
	CallAudioInput(false);
}

void UCFH_CharacterActionHandler::CallAudioInput(bool ObjectRequest) const
{
	UE_LOG(LogTemp, Warning, TEXT("CallAudioInput Called"))
	if (GI->IsSpeakActive == 1.f)
	{
		// Check if it is a NPCSpeak sequence
		if (!ObjectRequest)
		{
			if (ProceedWithAction(FString("Audio")))
			{
				// Pass active dialogue, clip, and if current audio can be overriden
				CharacterSpeaker->PlayObjectAudio(GI->CurrentlyActiveObject->ActiveDialogue, GI->CurrentlyActiveObject->ActiveAudioClip, GI->CurrentlyActiveObject->ActionOverride);
			}
		}
		else if (ProceedWithAction(FString("Object")))
		{
			// Pass active dialogue, clip, and if current audio can be overriden
			CharacterSpeaker->PlayObjectAudio(GI->CurrentlyActiveObject->ActiveDialogue, GI->CurrentlyActiveObject->ActiveAudioClip, GI->CurrentlyActiveObject->ActionOverride);
			UE_LOG(LogTemp, Warning, TEXT("Called NPC Speak audio"))
		}
	}
}