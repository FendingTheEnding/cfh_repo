// Fill out your copyright notice in the Description page of Project Settings.


#include "MyGameInstanceCpp.h"
#include "ActionObjectModel.h"
#include "ActionSequenceSetter.h"
#include "Engine/DataTable.h"
#include "Materials/MaterialParameterCollection.h"


UMyGameInstanceCpp::UMyGameInstanceCpp(const FObjectInitializer& ObjectInitializer)//: Super(ObjectInitializer)
{
	// Gets DataTable containting reference information for all interactable objects
	static ConstructorHelpers::FObjectFinder<UDataTable> ActionGuideDTObject(TEXT("DataTable'/Game/MyContent/Audio/CFH_ActionGuide.CFH_ActionGuide'"));
	if (ActionGuideDTObject.Succeeded())
	{
		ActionGuideDT = ActionGuideDTObject.Object;
	}
}


void UMyGameInstanceCpp::SetSpeak(float val)
{
	IsSpeakActive = val;
}

FActionGuide* UMyGameInstanceCpp::GetRequestedReference(FName StaticMeshName, UActionSequenceSetter* RequestingActor)//, FVector MeshLocation, FRotator MeshRotation, float SpeakDistance)
{
	// Assign action guide for requesting object
	FActionGuide* RequestedReference = ActionGuideDT->FindRow<FActionGuide>(StaticMeshName, ContextString);

	// Add object reference number and VPRA component for array for external sequence assignments
	ReferenceNumArr.Add(FParse::HexNumber(*RequestedReference->ReferenceNumber));
	//ViewPosRelActArr.Add(RequestingComponent);
	ActionObjectArray.Add(RequestingActor);
	/*
	FObjectInnerArr AddToArray;
	for (float Value : { MeshLocation.X, MeshLocation.Y, MeshLocation.Z, MeshRotation.Roll, MeshRotation.Pitch, MeshRotation.Yaw, SpeakDistance })
	{
		AddToArray.InnerArray.Add(Value);
	}
	ObjectLocationRotationArr.Add(AddToArray);
	*/

	return RequestedReference;
}

// MOVE: Set in GameInstance
void UMyGameInstanceCpp::AssignCurrentObject(AActionObjectModel* ObjectToBeAssigned)
{
	CurrentlyActiveObject = ObjectToBeAssigned;
	UE_LOG(LogTemp, Warning, TEXT("CurrentlyActiveObject succeeded!!"));
}
// */