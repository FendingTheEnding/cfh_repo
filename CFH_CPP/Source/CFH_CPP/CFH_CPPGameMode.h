// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CFH_CPPGameMode.generated.h"

UCLASS(minimalapi)
class ACFH_CPPGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACFH_CPPGameMode();
};



