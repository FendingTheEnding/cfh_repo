// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "CFH_CPPHUD.generated.h"

UCLASS()
class ACFH_CPPHUD : public AHUD
{
	GENERATED_BODY()

public:
	ACFH_CPPHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

