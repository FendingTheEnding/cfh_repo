// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PlacementByRotation.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ASSETPLAYGROUND_API UPlacementByRotation : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPlacementByRotation();

private:
	void FindMeshes();
	void CreateMeshes();
	void PlaceMeshes();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	// Number of rows to be constructed
	UPROPERTY(EditAnyWhere, Category = "Setup")
	int32 MeshNumber = 8;
	// Radius of circle for meshes to be placed
	UPROPERTY(EditAnyWhere, Category = "Setup")
	float Radius = 1.f;
	// Angle between each column
	UPROPERTY(EditAnyWhere, Category = "Setup")
	float StartAngle = 1.f;
	// Rotation object column one should be at
	UPROPERTY(EditAnyWhere, Category = "Setup")
	float ObjectRotation = 1.f;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	UStaticMeshComponent* MainMesh;

	UPROPERTY(EditAnywhere, Category = "Array")
	TArray<UStaticMeshComponent*> SubMeshArray;

	UStaticMesh* Asset;

		
};
