// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HydroStand.generated.h"

UCLASS()
class ASSETPLAYGROUND_API AHydroStand : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHydroStand();

private:
	void FindMeshes();
	void CreateMeshes();
	void PlaceMeshes();
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Number of rows to be constructed
	UPROPERTY(EditAnyWhere, Category = "Setup")
	int32 RowNumber = 8;
	// Number of columns to be constructed
	UPROPERTY(EditAnyWhere, Category = "Setup")
	int32 ColNumber = 8;
	// Radius of circle for meshes to be placed
	UPROPERTY(EditAnyWhere, Category = "Setup")
	float Radius = 1.f;
	// Offset between column one and two
	UPROPERTY(EditAnyWhere, Category = "Setup")
	float OffSet = 1.f;
	// Angle between each column
	UPROPERTY(EditAnyWhere, Category = "Setup")
	float StartAngle = 1.f;
	// Rotation object column one should be at
	UPROPERTY(EditAnyWhere, Category = "Setup")
	float ObjectRotation = 1.f;
	// Hight of base row
	UPROPERTY(EditAnyWhere, Category = "Setup")
	float Height = 1;
	// Hight from base row to next
	UPROPERTY(EditAnyWhere, Category = "Setup")
	float HeightToNext = 1;



public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	UStaticMeshComponent* MainMesh;
	//UPROPERTY(VisibleAnywhere, Category = "Components")
	//UStaticMeshComponent* Plant_0_0;

	UPROPERTY(EditAnywhere, Category = "Array")
	TArray<UStaticMeshComponent*> PlantMeshArray;

	UStaticMesh* Asset;

};
