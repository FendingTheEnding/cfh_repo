// Fill out your copyright notice in the Description page of Project Settings.


#include "MeshPlacementByRotation.h"

// Sets default values for this component's properties
UMeshPlacementByRotation::UMeshPlacementByRotation()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	//MainMesh = CreateDefaultSubobject<UStaticMeshComponent>(FName("MainMesh"));
	//GetOwner()->SetRootComponent(MainMesh);
	//Plant_0_0 = CreateDefaultSubobject<UStaticMeshComponent>(FName("Plant_0_0"));
	//CreatePlantMeshes();
	//static ConstructorHelpers::FObjectFinder<UStaticMesh> AssetA(TEXT("StaticMesh'/Game/PN_WildBerries/Meshes/Blackberry/Blackberry_01.Blackberry_01'"));

	// ...
}

// Called when the game starts
void UMeshPlacementByRotation::BeginPlay()
{
	Super::BeginPlay();

	// ...
	CreatePlantMeshes();
}

/*
// Called every frame
void UPlacementByRotation::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}
*/

void UMeshPlacementByRotation::CreatePlantMeshes()
{
	for (int32 i = 0; i < MeshNumber; i++)
	{
		FString MeshString = TEXT("SubMesh_");
		MeshString.AppendInt(i);
		FName MeshName = FName(*MeshString);

		FName OwnerName = GetOwner()->GetFName();// ->CreateDefaultSubobject<UStaticMeshComponent>(MeshName);

		//UPROPERTY(VisibleAnywhere, Category = "Components")
		//SubMeshArray.Add(GetOwner()->CreateDefaultSubobject<UStaticMeshComponent>(MeshName));
		//CurMesh->AttachTo(MainMesh);

		//PlantMeshArray.Add(CurMesh);
		UE_LOG(LogTemp, Warning, TEXT("%s"), *OwnerName.ToString());
	}
}

void UMeshPlacementByRotation::PlaceMeshes()
{
	/*
	FVector ActLoc = GetOwner()->GetActorLocation();
	FRotator ActRot = GetOwner()->GetActorRotation();
	float RowAngle = 360.f / MeshNumber;
	FVector Pos;
	for (float i = 0; i < SubMeshArray.Num(); i++)
	{
		Pos = { Radius*sinf((StartAngle + j * RowAngle)*(PI / 180)), Radius*cosf((StartAngle + j * RowAngle)*(PI / 180)), 0.f };
		Pos += ActLoc;
		FRotator Rot = { 0.f, ObjectRotation + j * RowAngle, 0.f };
		Rot += ActRot;
		UStaticMeshComponent* CurMesh = SubMeshArray[i];
		CurMesh->SetRelativeLocationAndRotation(Pos, Rot);
		CurMesh->SetStaticMesh(Asset);
		//UE_LOG(LogTemp, Warning, TEXT("%f, %f, %f, %f"), Pos.X, Pos.Y, Pos.Z, RowAngle);
	}
	// */
}