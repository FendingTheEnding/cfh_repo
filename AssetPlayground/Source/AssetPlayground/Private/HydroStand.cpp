// Fill out your copyright notice in the Description page of Project Settings.


#include "HydroStand.h"
#include "UObject/ConstructorHelpers.h"

// Sets default values
AHydroStand::AHydroStand()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MainMesh = CreateDefaultSubobject<UStaticMeshComponent>(FName("MainMesh"));
	SetRootComponent(MainMesh);
	//Plant_0_0 = CreateDefaultSubobject<UStaticMeshComponent>(FName("Plant_0_0"));
	FindMeshes();
	CreateMeshes();
	static ConstructorHelpers::FObjectFinder<UStaticMesh> AssetA(TEXT("StaticMesh'/Game/PN_WildBerries/Meshes/Blackberry/Blackberry_01.Blackberry_01'"));
	Asset = AssetA.Object;

}

// Called when the game starts or when spawned
void AHydroStand::BeginPlay()
{
	Super::BeginPlay();

	PlaceMeshes();
	
}

// Called every frame
void AHydroStand::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AHydroStand::FindMeshes()
{
	/*
	TArray<UMeshPlacementByRotation> CompArr;
	GetComponentsByClass<UMeshPlacementByRotation>(CompArr);
	if (CompArr[0])
	{
		UMeshPlacementByRotation* CurMesh = CompArr[0];
		FName Namer = CurMesh->GetFName();

		UE_LOG(LogTemp, Warning, TEXT("%s"), *Namer.ToString());

	}
	*/
}

void AHydroStand::CreateMeshes()
{

	/*
	for (int32 i = 0; i < ColNumber; i++)
	{
		for (int32 j = 0; j < RowNumber; j++)
		{
			FString MeshString = TEXT("Plant_");
			MeshString.AppendInt(j);
			MeshString.Append(TEXT("_"));
			MeshString.AppendInt(i);
			FName MeshName = FName(*MeshString);

			UPROPERTY(VisibleAnywhere, Category = "Components") 
			PlantMeshArray.Add(CreateDefaultSubobject<UStaticMeshComponent>(MeshName));
			//CurMesh->AttachTo(MainMesh);

			//PlantMeshArray.Add(CurMesh);
			//UE_LOG(LogTemp, Warning, TEXT("%s"), *MeshString);
		}
	}
	*/
}

void AHydroStand::PlaceMeshes()
{
	FVector ActLoc = GetActorLocation();
	FRotator ActRot = GetActorRotation();
	float RowAngle = 360.f / RowNumber;
	int32 j = 0;
	int32 k = 0;
	FVector Pos;
	for (float i = 0; i < PlantMeshArray.Num(); i++)
	{
		if (j % 2 == 0) 
		{
			Pos = { Radius*sinf((StartAngle + j*RowAngle)*(PI / 180)), Radius*cosf((StartAngle + j*RowAngle)*(PI / 180)), Height + k*HeightToNext };
		}
		else
		{
			Pos = { Radius*sinf((StartAngle + j*RowAngle)*(PI / 180)), Radius*cosf((StartAngle + j*RowAngle)*(PI / 180)), OffSet + Height + k*HeightToNext };
		}
		Pos += ActLoc;
		FRotator Rot = { 0.f, ObjectRotation + j*RowAngle, 0.f };
		Rot += ActRot;
		UStaticMeshComponent* CurMesh = PlantMeshArray[i];
		CurMesh->SetRelativeLocationAndRotation(Pos, Rot);
		CurMesh->SetStaticMesh(Asset);
		j += 1;
		if (j >= RowNumber)
		{
			k += 1;
			j = 0;
		}
		//UE_LOG(LogTemp, Warning, TEXT("%f, %f, %f, %f"), Pos.X, Pos.Y, Pos.Z, RowAngle);
	}
	// */
}