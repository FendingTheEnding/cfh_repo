// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "AssetPlaygroundGameMode.h"
#include "AssetPlaygroundHUD.h"
#include "AssetPlaygroundCharacter.h"
#include "UObject/ConstructorHelpers.h"

AAssetPlaygroundGameMode::AAssetPlaygroundGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AAssetPlaygroundHUD::StaticClass();
}
